Short platformer inspired on Little Samson and the buggy earlier Rockman enemy spawn system.

The original idea was bigger but got cut due to time constraints turned from a game to the introduction of a game.



Basic controls:

A/left: move left.

D/right: move right.

W/up: jump.

H/space: shot (some characters can't shot mid air).



Character specific controls:

Ninja: 

W/up while jumping against a wall: Climb

W/Up on a wall: move up.

S/Down on a wall: move down.

Leave wall by normal walk jump (move left/right+jump)

Gunner:

W/up while jumping: shot down and get a second jump impulse.

Mage:

Press+hold W/up while jumping: float for awhile.

Press+hold shot button: charge shot.

Knight:

Nothing, but can walk on spikes.



Cheats!

Even if the main intended feature is not used (and not finished), can be unlocked with the key "9", then with keys 1 to 4 you can change your character.



The game was made for the Video Game Programming 2 class of the Video Game Design and Programming course (a kind of associate? degree) at the Facultad de Ingenier�a y Ciencias H�dricas, Universidad del Litoral, Santa Fe, Argentina.



A game by +>eons. 
Source code license is MIT.


Assets credits:

Characters:
Player characters based on the Classic Hero variants by Disthron:
http://opengameart.org/users/disthron
Princess by Jason-Em: http://opengameart.org/content/bushly-and-princess-sera

Enemies:
Bat based on ansimuz Chibi Monster set by ansimuz:
http://opengameart.org/content/chibi-monsters-set-01
Skeleton based on Disthron's work: http://opengameart.org/content/skeleton-guy-animated
The rest based on Jason-Em's work: http://opengameart.org/users/grafxkid

Tilesets
Dark Forest Town by Disthron: http://opengameart.org/content/dark-forest-town-tile-set-0
Beastlands by surt: http://opengameart.org/content/beastlands
Simple broad purpose tileset by surt and others: http://opengameart.org/content/simple-broad-purpose-tileset


Explosions and gun effect by ansimuz: http://opengameart.org/content/animated-explosions


Background and title images:
Countryside and Mountain at dusk by ansimuz: http://opengameart.org/users/ansimuz


Emojis from twemoji: https://twitter.github.io/twemoji/


Blur and scanline shader from HaxeFlixel's Filters demo:
Site: https://github.com/HaxeFlixel/flixel-demos
Film Grain post-process by Martins Upitis
Site: devlog-martinsh.blogspot.com
Perlin noise shader by toneburst:
Site:http://machinesdontcare.wordpress.com/2009/06/25/3d-perlin-noise-sphere-vertex-shader-sourcecode


Sound effects by Damaged Panda
Site: http://opengameart.org/users/damaged-panda
SFX pack: http://opengameart.org/content/100-plus-game-sound-effects-wavoggm4a


Music by RevampedPRO
Site: http://opengameart.org/users/revampedpro
Music pack: http://opengameart.org/content/the-slimekings-tower-ost-retro-like
Ending song: http://opengameart.org/content/orchestral-adventure


Title font is OpenDyslexic: https://github.com/antijingoist/open-dyslexic/


Powered by HaxeFlixel: http://haxeflixel.com/