package gamestates;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxText;

/*
*	FlxState que se utilizó para englobar cutscenes y playstate.
*/

@:allow(util.TiledLevel)
@:allow(enemies.Spawner)
class CHPState extends FlxState
{

	override public function create():Void
	{
		super.create();

		border = new FlxSprite(0,0);
		flixel.util.FlxSpriteUtil.drawRect(border.makeGraphic(FlxG.camera.width,FlxG.camera.height,flixel.util.FlxColor.TRANSPARENT)
			,0,0,FlxG.camera.width,FlxG.camera.height,flixel.util.FlxColor.TRANSPARENT,
			{color:flixel.util.FlxColor.BLACK,thickness:10}
		);
		border.active = false;
		border.solid = false;
		border.scrollFactor.x = 0;
		border.scrollFactor.y = 0;

		FlxG.camera.fade(flixel.util.FlxColor.BLACK,1,true);

	}

	private var level:util.TiledLevel;
	private var border:FlxSprite;

}
