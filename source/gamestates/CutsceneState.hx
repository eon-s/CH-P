package gamestates;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;

import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;

import flixel.group.FlxSpriteGroup;
import flixel.group.FlxGroup;

import flixel.util.FlxTimer;

import util.TiledLevel;

/*
* Intento de crear cutscenes, podrian ser un FlxBasic, FlxObject o FlxSubstate y funcionar igual o mejor
* Las secuencias de eventos son temporizadores con funciones asignadas.
* Debería ser posible hacerlo dinamico.
* La forma de trabajo se puede apreciar mejor en gamestates.IntroState.
* Resultó bastante organizado pero se puede mejorar mucho.
*/

class CutsceneState extends CHPState
{

  public var followPoint = new flixel.FlxObject(0,0);
  public var actors:FlxSpriteGroup;
  public var effects:FlxSpriteGroup;
  public var dialogs:FlxSpriteGroup;

  override public function create():Void
  {
    super.create();

    add(followPoint);
    FlxG.camera.follow(followPoint);

  }


	override public function destroy():Void
	{
    clear();
		super.destroy();

	}


	override public function update(elapsed:Float):Void
	{

    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.ESCAPE,
                                  flixel.input.keyboard.FlxKey.SPACE,
                                  flixel.input.keyboard.FlxKey.H]))
    {
      finished = true;
    }

    if (!timer.active)
    {
      currentSeq++;
      if ((currentSeq)<sequenceEvents.length)
        loadSequence();
      else
        finished = true;
    }

		super.update(elapsed);

	}


  private function loadSequence()
  {
    var loops:Int = 0;
    var time:Float = 1;

    if (currentSeq<sequenceLoops.length)
      loops = sequenceLoops[currentSeq];

    if (currentSeq<sequenceTime.length)
      time = sequenceTime[currentSeq];

    timer.start(time,sequenceEvents[currentSeq], loops);

  }


  private var finished=false;
  private var started=false;
  private var timer:FlxTimer = new FlxTimer();

  private var sequenceTime:Array<Float> = new Array<Float>();
  private var sequenceEvents:Array<FlxTimer->Void> = new Array<FlxTimer->Void>();
  private var sequenceLoops:Array<Int> = new Array<Int>();
  private var currentSeq = -1;

}
