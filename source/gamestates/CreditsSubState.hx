package gamestates;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxColor;

/*
*	Unico substate utilizado, muestra los créditos en pantalla.
*/

class CreditsSubState extends FlxSubState
{

	override public function create():Void
	{
		super.create();

		var midScreen = FlxG.camera.width*0.5;
		var background = new FlxSprite(0,0);
		background.makeGraphic(FlxG.camera.width,FlxG.camera.height,0xA0000000);

		titleTxt = new FlxText(midScreen-40, FlxG.camera.height *0.5 - 50,80);
		titleTxt.scrollFactor.set(0, 0);
		titleTxt.borderColor = 0xff000000;
		titleTxt.borderStyle = SHADOW;
		titleTxt.alignment = CENTER;
		titleTxt.text = "Credits";

		creditTxt = new FlxText(midScreen-80, FlxG.camera.height *0.5 - 30 ,160);
		creditTxt.scrollFactor.set(0, 0);
		creditTxt.borderColor = 0xff000000;
		creditTxt.borderStyle = SHADOW;
		creditTxt.alignment = CENTER;
		creditTxt.size = 8;
		creditTxt.text = "A game by +>eons\n\nCharacters and tiles by ansimuz, Disthron, Jason-Em, surt and others\n SFX and BGM by Damaged Panda and RevampedPRO \n\nMore details in the README file";

		exitTxt = new FlxText(midScreen-40, FlxG.camera.height *0.5 + 70 ,80);
		exitTxt.scrollFactor.set(0, 0);
		exitTxt.borderColor = 0xff000000;
		exitTxt.borderStyle = SHADOW;
		exitTxt.alignment = CENTER;
		exitTxt.text = "EXIT";

		selectionRect = new FlxSprite(exitTxt.x+20,exitTxt.y);
		selectionRect.makeGraphic(40,14,FlxColor.TRANSPARENT);
		flixel.util.FlxSpriteUtil.drawRoundRect(selectionRect,0,0,40,14,7,7,0x77ffff00);
		selectionRect.scale.x = 0.8;

		add(background);
		add(titleTxt);

		add(selectionRect);

		add(creditTxt);
		add(exitTxt);

	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		if (FlxG.keys.justPressed.ANY)
		{
			close();
		}
	}

	private var startTxt:FlxText;
	private var creditTxt:FlxText;
	private var titleTxt:FlxText;
	private var exitTxt:FlxText;
	private var selectionRect:FlxSprite;

}
