package gamestates;

import flixel.FlxG;
import flixel.FlxSprite;

import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;

import flixel.group.FlxSpriteGroup;

import flixel.util.FlxTimer;
import util.Dialog;
import util.DialogMaker;

/*
* El primer nivel, no cargado dinamicamente aunque los tmx poseen información para ello.
* Se podría mejorar el dinamismo si CutsceneState fuese tipo FlxObject o FlxBasic.
*/

class IntroState extends CutsceneState
{

  override public function create():Void
  {
    super.create();

    actors = new FlxSpriteGroup();
    effects = new FlxSpriteGroup();
    dialogs = new FlxSpriteGroup();

    //Load actors
    kingActor = new FlxSprite(32,240,"assets/images/intro/introKing.png");
    kingActor.offset.y = 6;

    pikeActor = new FlxSprite(112,257);
    pikeActor.loadGraphic("assets/images/intro/introPikeman.png", true, 24, 24);
    pikeActor.animation.add("idle", [0,1,2,3], 4, true);
    pikeActor.offset.y = 8;
    pikeActor.flipX = true;

    actors.add(kingActor);
    actors.add(pikeActor);

    var background = new FlxSprite(0,16,"assets/images/intro/background.png");

    add(background);
    add(actors);
    add(effects);

    add(new FlxSprite(0,16,"assets/images/intro/foreground.png"));
    add(dialogs);

    followPoint.setPosition(0,background.height*0.5);

    createSequences();

    FlxG.camera.setScrollBoundsRect(0,0,background.width,background.height+16);

    add(border);

    FlxG.sound.playMusic("assets/bgm/DungeonTheme_0-Intro.ogg", 1, true);

  }


	override public function destroy():Void
	{
    clear();
		super.destroy();

	}


	override public function update(elapsed:Float):Void
	{

    if (finished)
    {
      FlxG.camera.fade(flixel.util.FlxColor.BLACK,1,false,function(){Reg.currentLevel++;FlxG.switchState(new PlayState());});
    }
		super.update(elapsed);

	}


  private function createSequences()
  {

    sequenceLoops =  [10,
                            5
                            ];

    sequenceEvents.push(seq0);

    sequenceEvents.push(seqFinal);

  }

  //the search for the heroes
  private function seq0(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        dlg = DialogMaker.makeDialog(kingActor,2,2,[13,15,14]);
        dialogs.add(dlg);
      case 3:
        dlg = DialogMaker.makeDialog(kingActor,4,2,[48,10,7,49,30]);
        dialogs.add(dlg);
      case 5:
        dlg = DialogMaker.makeDialog(pikeActor,1,1,[51]);
        dialogs.add(dlg);
      case 7:
        dlg = DialogMaker.makeDialog(kingActor,1,1,[28],9);
        dialogs.add(dlg);
      case 9:
        dlg = DialogMaker.makeDialog(pikeActor,3,2,[24,5,7,50]);
        dialogs.add(dlg);
      default: null;
    }

  }


  private function seqFinal(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 5:
        FlxG.sound.music.stop();
      default:
        null;
    }

  }

  private var kingActor:FlxSprite;
  private var pikeActor:FlxSprite;
  private var dlg:FlxSprite;

}
