package gamestates;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.group.FlxGroup;
import flixel.FlxObject;

/*
*	FlxState que representa el menu principal
*/

class MenuState extends FlxState
{

	override public function create():Void
	{
		super.create();

		FlxG.mouse.visible = false;

		var midScreen = FlxG.camera.width*0.5;
		var background = new FlxSprite(0,0,"assets/images/title/background.png");
		menuGroup = new FlxTypedGroup<FlxObject>();

		titleTxt = new FlxText(midScreen-120,20,240);
		titleTxt.setFormat("assets/fonts/OpenDyslexicAlta-Bold.ttf",24);
		titleTxt.scrollFactor.set(0, 0);
		titleTxt.borderColor = 0xff000000;
		titleTxt.borderStyle = SHADOW;
		titleTxt.alignment = CENTER;
		titleTxt.text = "CLASSIC HEROES:\nPROLOGUE";

		startTxt = new FlxText(midScreen-40, FlxG.camera.height *0.5 - 10 , 80);
		startTxt.scrollFactor.set(0, 0);
		startTxt.borderColor = 0xff000000;
		startTxt.alignment = CENTER;
		startTxt.borderStyle = SHADOW;
		startTxt.text = "START";

		creditTxt = new FlxText(midScreen-40, FlxG.camera.height *0.5 + 10 ,80);
		creditTxt.scrollFactor.set(0, 0);
		creditTxt.borderColor = 0xff000000;
		creditTxt.borderStyle = SHADOW;
		creditTxt.alignment = CENTER;
		creditTxt.text = "CREDITS";

		exitTxt = new FlxText(midScreen-40, FlxG.camera.height *0.5 + 30 ,80);
		exitTxt.scrollFactor.set(0, 0);
		exitTxt.borderColor = 0xff000000;
		exitTxt.borderStyle = SHADOW;
		exitTxt.alignment = CENTER;
		exitTxt.text = "EXIT";

		selectionRect = new FlxSprite(startTxt.x+20,startTxt.y);
		selectionRect.makeGraphic(40,14,FlxColor.TRANSPARENT);
		flixel.util.FlxSpriteUtil.drawRoundRect(selectionRect,0,0,40,14,7,7,0x77ffff00);

		menuGroup.add(selectionRect);
		menuGroup.add(startTxt);
		menuGroup.add(creditTxt);
		menuGroup.add(exitTxt);

		add(background);
		add(titleTxt);

		add(selectionRect);
		add(startTxt);
		add(creditTxt);
		add(exitTxt);

		FlxG.addPostProcess( new flixel.effects.postprocess.PostProcess("assets/shaders/scanline.frag"));
		FlxG.addPostProcess( new flixel.effects.postprocess.PostProcess("assets/shaders/grain.frag"));
		FlxG.addPostProcess( new flixel.effects.postprocess.PostProcess("assets/shaders/blur.frag"));

		var border:FlxSprite;
		border = new FlxSprite(0,0);

		flixel.util.FlxSpriteUtil.drawRect(border.makeGraphic(FlxG.camera.width,FlxG.camera.height,flixel.util.FlxColor.TRANSPARENT)
			,0,0,FlxG.camera.width,FlxG.camera.height,flixel.util.FlxColor.TRANSPARENT,
			{color:flixel.util.FlxColor.BLACK,thickness:10});
		add(border);

		var eonsTxt = new FlxText(FlxG.camera.width-30, FlxG.camera.height - 18 , 30);
		eonsTxt.scrollFactor.set(0, 0);
		eonsTxt.borderColor = 0xff000000;
		eonsTxt.borderStyle = SHADOW;
		eonsTxt.alignment = CENTER;
		eonsTxt.size = 8;
		eonsTxt.text = "+>e";
		add(eonsTxt);

	}


	override public function update(elapsed:Float):Void
	{

		if (FlxG.keys.anyJustPressed([NINE]))
		{
			Reg.muteSounds = !Reg.muteSounds;
		}

		if (FlxG.keys.anyJustPressed([W,UP]))
		{
			selection-=1;
			if (selection<0)
				selection = 2;
			selectionChanged = true;
		}
		else if (FlxG.keys.anyJustPressed([S,DOWN]))
		{
			selection+=1;
			if (selection>2)
				selection = 0;
			selectionChanged = true;
		}
		else if (FlxG.keys.anyJustPressed([SPACE,ENTER]))
		{
			switch (selection)
			{
				case 0:
					//Reg.currentLevel=2;
					Reg.currentLevel++;
					FlxG.switchState(new gamestates.IntroState());
					//FlxG.switchState(new gamestates.EndingState());
				case 1:
					menuGroup.forEach(function(each){each.visible = false;});
					var subst = new CreditsSubState();
					subst.closeCallback = function(){menuGroup.forEach(function(each){each.visible = true;});};
					openSubState(subst);
				case 2:
					openfl.system.System.exit(0);
				default: null;
			}
		}

		if (selectionChanged)
		{
			var destY:Float = 0;
			var sclX:Float = 1;
			switch(selection)
			{
				case 1:
					destY = creditTxt.y;
					sclX = 1.2;
				case 2:
					destY = exitTxt.y;
					sclX = 0.8;
				default:
					destY = startTxt.y;
					sclX = 1;
			}

			flixel.tweens.FlxTween.tween(selectionRect,{y:destY},0.4);
			flixel.tweens.FlxTween.tween(selectionRect.scale,{x:sclX},0.4);
		}

		selectionChanged = false;
		super.update(elapsed);

	}

	private var startTxt:FlxText;
	private var creditTxt:FlxText;
	private var titleTxt:FlxText;
	private var exitTxt:FlxText;
	private var selection:Int = 0;
	private var selectionRect:FlxSprite;
	private var selectionChanged = false;

	private var menuGroup:FlxTypedGroup<FlxObject>;

}
