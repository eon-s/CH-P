package gamestates;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxText;
import gameui.Health;

/*
*	FlxState/CHPState sencillo para cargar niveles comunes
*/

@:allow(util.TiledLevel)
@:allow(enemies.Spawner)
class PlayState extends CHPState
{

	override public function create():Void
	{
		super.create();
		resetTime = new flixel.util.FlxTimer();
		lockedBag = false;
		youDied = false;

		bgColor = 0xffaaaaaa;

		bag = new characters.Bag();
		killzone = new FlxGroup();
		levelLimits = new FlxGroup();
		damageZones = new FlxGroup();
		spawners = new FlxGroup();
		enemies = new FlxGroup();
		shots = new FlxGroup();
		effects = new FlxGroup();

		level = new util.TiledLevel("assets/tiled/" + Reg.levels[Reg.currentLevel] +".tmx", this);

		// Add static images
		add(level.imagesLayer);

		// Load player objects
		add(level.bkgObjectsLayer);

		// Add backgrounds
		add(level.backgroundLayer);

		add(player);
		add(level.solidTiles);

		add(level.frgObjectsLayer);
		add(level.foregroundLayer);

		add(enemies);
		add(shots);
		add(effects);

		Reg.playerShot = shots;
		Reg.enemyGroup = enemies;
		Reg.effectGroup = effects;

		add(border);

		FlxG.camera.bgColor = 0xff000000;

		add(bag.activeHealth());

	}


	override public function update(elapsed:Float):Void
	{
		if (Reg.firstPlay)
		{
			Reg.firstPlay = false;
			add(util.DialogMaker.makeDialog(player,3,2,[24,5,7,47],1));
		}

		//Desactivado es estos niveles, funciona pero le faltan efectos y corrección de UI "Health"
		if (!lockedBag)
		{
			if (FlxG.keys.justPressed.ONE)
			{
      	remove(player);
				remove(bag.activeHealth());
      	player = bag.change(1);
      	add(player);
				add(bag.activeHealth());
      	FlxG.camera.follow(player);
    	}
			else if (FlxG.keys.justPressed.TWO)
			{
      	remove(player);
				remove(bag.activeHealth());
      	player = bag.change(2);
      	add(player);
				add(bag.activeHealth());
      	FlxG.camera.follow(player);
    	}
			else if (FlxG.keys.justPressed.THREE)
			{
      	remove(player);
				remove(bag.activeHealth());
      	player = bag.change(3);
      	add(player);
				add(bag.activeHealth());
      	FlxG.camera.follow(player);
    	}
			else if (FlxG.keys.justPressed.FOUR)
			{
      	remove(player);
				remove(bag.activeHealth());
      	player = bag.change(4);
      	add(player);
				add(bag.activeHealth());
      	FlxG.camera.follow(player);
    	}
		}
		else if (FlxG.keys.justPressed.NINE) //cheat
		{
			lockedBag = false;
		}

		super.update(elapsed);

		level.collideWithLevel(player);
		shots.forEach(function(each)
		{
			var ps:characters.PlayerShot = cast(each,characters.PlayerShot);
			if (!ps.ignoreWall){
				if (level.collideWithLevel(ps)) ps.kill();
			}
		});

		enemies.forEachAlive(function(each)
		{
			try
			{
				var eb:enemies.EnemyBase = cast(each,enemies.EnemyBase);
				if (!eb.ignoreWall)
				{
					level.collideWithLevel(eb);
				}
			}
			catch (e:Dynamic)
			{
				trace(e);
			}

		});

		FlxG.overlap(Reg.playerShot,Reg.enemyGroup,shotEnemy);
		FlxG.overlap(player,Reg.enemyGroup,playerEnemy);

		FlxG.overlap(exit, player, win);

		if ((FlxG.overlap(killzone, player)||player.health<=0)&&!youDied)
		{
			youDied = true;
			player.kill();

			resetTime.start(1,function(t:flixel.util.FlxTimer){FlxG.resetState();},1);
		}
		else if (FlxG.overlap(levelLimits, player))
		{
			player.x = player.last.x;
		}
		else if (FlxG.collide(damageZones, player))
		{
			player.spikeHurt(2);
		}

	}


	public function win(Exit:FlxObject, Player:FlxObject):Void
	{
		Reg.currentLevel = exit.nextLevel;
		if (exit.isCutscene)
			if (exit.nextLevel == Reg.levels.length-1)
				FlxG.switchState(new EndingState());
			else FlxG.switchState(new CutsceneState());
		else
			FlxG.switchState(new PlayState());

		Reg.firstPlay = true;

	}


	private function shotEnemy(sht:FlxObject,enm:FlxObject)
	{
		if (!cast(enm,enemies.EnemyBase).ignoreShots)
		{
			var temph = enm.health;
			var tempshot = cast(sht,characters.PlayerShot);

			enm.hurt(tempshot.strength);
			tempshot.hurt(temph);
		}

	}


	private function playerEnemy(sht:FlxObject,enm:FlxObject)
	{
		var tempchar = cast(sht,characters.CharacterBase);

		tempchar.hurt(enm.health);
		if (cast(enm,enemies.EnemyBase).deadTouch) enm.hurt(1);

	}


	private function forceCharacter(ch:Int=1)
	{
    player = bag.change(ch);
		FlxG.camera.follow(player);

	}


	private static var youDied:Bool = false;

	private var levelLimits:FlxGroup;
	private var bag:characters.Bag;
	private var player:characters.CharacterBase;
	private var killzone:FlxGroup;
	private var damageZones:FlxGroup;
	private var spawners:FlxGroup;
	private var enemies:FlxGroup;
	private var shots:FlxGroup;
	private var effects:FlxGroup;
	private var exit:obstacles.Exit;
	private var actors:FlxGroup;
	private var lockedBag:Bool;
	private var resetTime:flixel.util.FlxTimer;

}
