package gamestates;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.text.FlxText;

import haxe.io.Path;
import flixel.tile.FlxTilemap;
import flixel.addons.editors.tiled.TiledMap;

import flixel.group.FlxSpriteGroup;

import flixel.util.FlxTimer;
import util.Dialog;
import util.DialogMaker;

/*
* Si un CHPState carga el último nivel de la lista, lo considera el Cutscene final y carga este.
*/

class EndingState extends CutsceneState
{

  override public function create():Void
  {
    super.create();

    actors = new FlxSpriteGroup();
    effects = new FlxSpriteGroup();
    dialogs = new FlxSpriteGroup();

    //Load actors
    kingActor = new FlxSprite(32,240,"assets/images/intro/introKing.png");
    kingActor.offset.y = 6;
    prinActor = new FlxSprite(64,256,"assets/images/intro/introPrin.png");
    prinActor.offset.y = 8;
    prinActor.flipX = true;
    warriors = new FlxSpriteGroup();
    warriors.add(new FlxSprite(0,0));
    warriors.add(new FlxSprite(16,0));
    warriors.add(new FlxSprite(32,0));
    warriors.add(new FlxSprite(48,0));

    warriors.members[0].loadGraphic("assets/images/intro/introNinja.png", true, 20, 20);
    warriors.members[1].loadGraphic("assets/images/intro/introMage.png", true, 20, 20);
    warriors.members[2].loadGraphic("assets/images/intro/introGunner.png", true, 20, 20);
    warriors.members[3].loadGraphic("assets/images/intro/introKnight.png", true, 20, 20);

    warriors.forEach(function(each){
      each.animation.add("idle", [0], 1, false);
      each.animation.add("walk", [1,2,3,4,5,6], 10, true);
      each.animation.play("walk");
      each.offset.y = 4;
    });

    warriors.members[3].animation.add("walk2", [1,2,3,4,5,6], 1, true);
    warriors.members[3].animation.add("walk3", [8,9,10,11,12,13], 4, true);
    warriors.flipX = true;

    bagIn = (new FlxSprite()).loadGraphic("assets/images/intro/bagIn.png",true,20,20);
    bagIn.animation.add("puff",[0,1,2,3,4],false);
    bag = (new FlxSprite()).loadGraphic("assets/images/intro/bag.png");

    actors.add(kingActor);
    actors.add(prinActor);
    actors.add(warriors);

    var background = new FlxSprite(0,16,"assets/images/intro/background.png");
    warriors.setPosition(background.width,prinActor.y);

    add(background);

    add(actors);
    add(effects);

    add(new FlxSprite(0,16,"assets/images/intro/foreground.png"));
    add(dialogs);

    followPoint.setPosition(0,background.height*0.5);

    createSequences();

    FlxG.camera.setScrollBoundsRect(0,0,background.width,background.height+16);
    FlxG.camera.follow(followPoint);
    add(border);

    FlxG.sound.playMusic("assets/bgm/epicloop-Ending.ogg", 1, true);

  }


	override public function destroy():Void
	{

    clear();
		super.destroy();

	}

	override public function update(elapsed:Float):Void
	{

    if (finished)
    {
      FlxG.camera.fade(flixel.util.FlxColor.BLACK,1,false,function(){Reg.currentLevel= -1;FlxG.switchState(new MenuState());});
    }
		super.update(elapsed);

	}


  private function createSequences()
  {

    sequenceLoops =  [10,      10,     7,     11,    11,
                            12,
                            5
                            ];

    sequenceEvents.push(seq0);
    sequenceEvents.push(seq1);
    sequenceEvents.push(seq2);
    sequenceEvents.push(seq3);
    sequenceEvents.push(seq4);
    sequenceEvents.push(seq5);

    sequenceEvents.push(seqFinal);

  }

  //real argunment
  private function seq0(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        dlg = DialogMaker.makeDialog(prinActor,1,1,[0]);
        dialogs.add(dlg);
      case 3:
        dlg = DialogMaker.makeDialog(kingActor,1,1,[1]);
        dialogs.add(dlg);
      case 5:
        dlg = DialogMaker.makeDialog(prinActor,1,1,[2]);
        dialogs.add(dlg);
      case 7:
        dlg = DialogMaker.makeDialog(kingActor,1,1,[3]);
        dialogs.add(dlg);
      case 9:
        dlg = DialogMaker.makeDialog(kingActor,2,1,[4,5]);
        dialogs.add(dlg);
      default: null;
    }

  }

  //they arrive
  private function seq1(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        prinActor.flipX = false;
        flixel.tweens.FlxTween.tween(followPoint,{x:warriors.x},5);
      case 5:
        for (i in 0...4)
          warriors.members[i].animation.play("walk");
        flixel.tweens.FlxTween.tween(warriors,{x:prinActor.x+16*4},4.8,{
          onComplete:function(tw){
            warriors.members[2].animation.play("idle");
            warriors.members[3].animation.play("idle");
          }
        });
        flixel.tweens.FlxTween.tween(followPoint,{x:prinActor.x+16*5},6);
        flixel.tweens.FlxTween.tween(warriors.members[0],{x:prinActor.x+16*3},0.7,{startDelay:4.9});
        flixel.tweens.FlxTween.tween(warriors.members[1],{x:prinActor.x+16*4},0.7,{startDelay:4.9,
          onComplete:function(tw){
            warriors.members[0].animation.play("idle");
            warriors.members[1].animation.play("idle");
          }
        });
      default: null;
    }

  }

  //the situation
  private function seq2(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        prinActor.flipX = true;
        dlg = DialogMaker.makeDialog(kingActor,2,2,[6,-1,7,8]);
        dialogs.add(dlg);
      case 2:
        flixel.tweens.FlxTween.tween(prinActor,{x:prinActor.x+16*3},3,{
          onStart:function(tw){
              dlg = DialogMaker.makeDialog(prinActor,1,1,[9]);
              dialogs.add(dlg);
          }
        });
        flixel.tweens.FlxTween.tween(prinActor,{alpha:0},1,{startDelay:2}).then(
          flixel.tweens.FlxTween.tween(prinActor,{x:prinActor.x+16*9},0.1)
        );
      case 3:
        dlg = DialogMaker.makeDialog(kingActor,2,1,[10,11]);
        dialogs.add(dlg);
      case 5:
        dlg = DialogMaker.makeDialog(kingActor,3,3,[12,13,14,15,16,-1,17]);
        dialogs.add(dlg);
      default: null;
    }

  }

  //the challenge
  private function seq3(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        dlg = DialogMaker.makeDialog(prinActor,1,1,[18]);
        dialogs.add(dlg);
      case 2:
        dlg = DialogMaker.makeDialog(kingActor,3,2,[20,21,-1,7,22,7]);
        dialogs.add(dlg);
        dlg = DialogMaker.makeDialog(warriors.members[3],1,1,[19]);
        dialogs.add(dlg);
        warriors.members[3].flipX = false;
      case 4:
        dlg = DialogMaker.makeDialog(kingActor,2,3,[39,-1,38,36,-1,37]);
        dialogs.add(dlg);
        flixel.tweens.FlxTween.tween(warriors.members[3],{x:warriors.members[3].x+16*2},3);
        flixel.tweens.FlxTween.tween(warriors.members[3],{alpha:0},0.1,{startDelay:2.9});
        warriors.members[3].animation.play("walk");
      case 6:
        dlg = DialogMaker.makeDialog(kingActor,3,3,[36,37,-1,38,36,39,38]);
        dialogs.add(dlg);
      case 8:
        dlg = DialogMaker.makeDialog(kingActor,3,2,[37,38,39,36,37,39]);
        dialogs.add(dlg);
        dlg = DialogMaker.makeDialog(prinActor,1,1,[34]);
        dialogs.add(dlg);
      case 10:
        dlg = DialogMaker.makeDialog(kingActor,2,2,[36,39,38,36]);
        dialogs.add(dlg);
        dlg = DialogMaker.makeDialog(warriors.members[1],1,1,[35]);
        dialogs.add(dlg);
      case 11:
        bag.setPosition(kingActor.x,kingActor.y);
        effects.add(bag);
        flixel.tweens.FlxTween.quadMotion(bag,bag.x,bag.y,bag.x+16*3,bag.y-16*2,warriors.members[0].x,warriors.members[0].y,2,true,{ease:flixel.tweens.FlxEase.quadInOut,
          onComplete:function(tw){
            bag.visible = false;
          }
        }).start();
      default:
        null;
    }

  }

  //the prize
  private function seq4(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        dlg = DialogMaker.makeDialog(kingActor,3,3,[23,-1,-1,24,25,26,19]);
        dialogs.add(dlg);
      case 3:
        dlg = DialogMaker.makeDialog(warriors.members[0],1,1,[24]);
        dialogs.add(dlg);
        flixel.tweens.FlxTween.tween(warriors.members[3],{alpha:1},0.1);
        flixel.tweens.FlxTween.tween(warriors.members[3],{x:warriors.members[3].x-16*2},3,{
          onComplete:function(tw){
            warriors.members[3].animation.play("idle");
          }
        });
        warriors.members[3].flipX = true;
        warriors.members[3].animation.play("walk3");
      case 4:
        dlg = DialogMaker.makeDialog(warriors.members[1],1,1,[24]);
        dialogs.add(dlg);
      case 5:
        dlg = DialogMaker.makeDialog(warriors.members[2],1,1,[25]);
        dialogs.add(dlg);
      case 6:
        dlg = DialogMaker.makeDialog(warriors.members[3],2,1,[32,27]);
        dialogs.add(dlg);
      case 8:
        dlg = DialogMaker.makeDialog(kingActor,1,1,[28]);
        dialogs.add(dlg);
      case 10:
        dlg = DialogMaker.makeDialog(warriors.members[3],1,1,[29]);
        dialogs.add(dlg);
      default:
      null;
    }

  }

  //departure
  private function seq5(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
        dlg = DialogMaker.makeDialog(kingActor,1,1,[30]);
        dialogs.add(dlg);
      case 2:
        warriors.flipX = false;
        flixel.tweens.FlxTween.tween(followPoint,{x:followPoint.x+16*5},3);
        flixel.tweens.FlxTween.tween(warriors.members[0],{x:warriors.members[0].x+16*8},3,{
          onComplete:function(tw){
            warriors.members[0].animation.play("idle");
          }
        });
        flixel.tweens.FlxTween.tween(warriors.members[1],{x:warriors.members[1].x+16*8},3,{
          onComplete:function(tw){
            warriors.members[1].animation.play("idle");
          }
        });
        flixel.tweens.FlxTween.tween(warriors.members[2],{x:warriors.members[2].x+16*8},3,{
          onComplete:function(tw){
            warriors.members[2].animation.play("idle");
          }
        });
        flixel.tweens.FlxTween.tween(warriors.members[3],{x:warriors.members[3].x+16*3},7,{
          onComplete:function(tw){
            warriors.members[3].animation.play("idle");
          }
        });
        warriors.members[0].animation.play("walk");
        warriors.members[1].animation.play("walk");
        warriors.members[2].animation.play("walk");
        warriors.members[3].animation.play("walk3");
      case 4:
        dlg = DialogMaker.makeDialog(warriors.members[3],1,1,[31]);
        dialogs.add(dlg);
      case 5:
        warriors.members[2].visible = false;
        effects.add(bagIn);
        bagIn.x = warriors.members[2].x;
        bagIn.y = warriors.members[2].y;
        bagIn.animation.play("puff");
        bag.x = warriors.members[0].x;
        bag.y = warriors.members[0].y;
        bag.visible = true;
      case 6:
        warriors.members[1].visible = false;
        bagIn.x = warriors.members[1].x;
        bagIn.y = warriors.members[1].y;
        bagIn.animation.play("puff");
      case 7:
        warriors.members[0].animation.play("walk");
        warriors.members[3].visible = false;
        bagIn.visible = true;
        bagIn.x = warriors.members[3].x;
        bagIn.y = warriors.members[3].y;
        bagIn.animation.play("puff");
        flixel.tweens.FlxTween.tween(warriors.members[0],{x:warriors.members[0].x+16*8},5);
        flixel.tweens.FlxTween.tween(followPoint,{x:followPoint.x-64},5,{startDelay:1});
        bag.visible = false;
      case 12:
        dlg = DialogMaker.makeDialog(kingActor,2,1,[32,19]);
        dialogs.add(dlg);
      default:
        null;
    }

  }


  private function seqFinal(t:FlxTimer)
  {
    switch (t.elapsedLoops)
    {
      case 1:
         FlxG.sound.music.fadeOut(3,0);
      case 2:
        var txt:FlxText = new FlxText(160, FlxG.camera.height/2-70, 160);
        txt.scrollFactor.set(0, 0);
        txt.borderColor = 0xff000000;
        txt.borderStyle = SHADOW;
        txt.text = "TO BE CONTINUED...";
        add(txt);
      case 5:
        FlxG.sound.music.stop();
      default:
        null;
    }
  }

  private var kingActor:FlxSprite;
  private var prinActor:FlxSprite;
  private var warriors:FlxSpriteGroup;
  private var bagIn:FlxSprite;
  private var bag:FlxSprite;
  private var dlg:FlxSprite;

}
