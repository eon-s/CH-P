/*
* Clase utilizada para fácil acceso a elementos generales como listas de enemigos, disparos, etc.
* Se intentó evitar su uso pero la complejidad lo tornó imposible sin hacer demasiados castings,
* como se puede apreciar con los accesos a characters.Bag.
*/

class Reg {

  public static function loadLevelList()
  {
    var file:String = openfl.Assets.getText("assets/tiled/maplist.txt");
	  var lines:Array<String> = file.split("\n");// split in all lines
	  var line:String;

	  while (lines.length>0)
	  {
	    line= StringTools.replace(lines.shift(),"\r","");
	    if (line.length != 0 || line == "\r")
	    {
	      Reg.levels.push(line);
	    }
	  }

  }

  public static var levels:Array<Dynamic> = new Array<Dynamic>();

  public static var currentLevel = -1;
  public static var firstPlay = true;

  public static var playerShot:flixel.group.FlxGroup;
  public static var enemyGroup:flixel.group.FlxGroup;
  public static var effectGroup:flixel.group.FlxGroup;
  public static var genericRNG = new flixel.math.FlxRandom();

  public static var muteSounds:Bool = true;

}
