package obstacles;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

/*
* Obstáculo simple pero que puede tener varias posiciones en el tilemap,
* necesita correcciones de posición y hitbox.
*/

class Spike extends FlxSprite
{

  public function new(X: Float, Y: Float, rotation: Float, ?SimpleGraphic:FlxGraphicAsset)
  {

    if (SimpleGraphic == null)
    {
      super(X, Y);
      loadGraphic("assets/images/obstacles/spike1.png");
    }
    else
    {
      super(X, Y, SimpleGraphic);
    }

    this.immovable = true;
    angle = rotation;

    if (angle == 0 )
    {
      height = 4;
      offset.y = 4;
      y-=4;
    }
    else if (angle == 180 )
    {
      x-=16;
      height = 4;
    }
    else if (angle == -90 )
    {
      height = 16;
      width = 4;
      x-=4;
      y-=16;
      offset.x+=8;
      offset.y-=4;
    }
    else if (angle == 90 )
    {
      height = 16;
      width = 4;
      offset.x+=4;
      offset.y-=4;
    }

  }

}
