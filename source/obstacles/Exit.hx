package obstacles;

import flixel.FlxObject;

class Exit extends FlxObject
{
  //Pensado para niveles con varias salidas
  public var nextLevel = 0;
  public var isCutscene = false;

  public function new(X: Float, Y: Float, H:Float, W:Float, next: Int, isCS:Bool)
  {
    super(X,Y);
    height = H;
    width = W;

    nextLevel = next;
    isCutscene = isCS;

    active = false;
    visible = false;
  }

}
