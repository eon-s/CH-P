package characters;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.input.keyboard.FlxKey;

/*
* Personaje con la capacidad de prenderse de las paredes, fue algo que había intentado
* antes con muchos problemas y lo que me motivó a realizar la separación de comportamientos.
*/

 class Ninja extends CharacterBase
 {

   public function new(X: Float, Y: Float)
   {
     super(X, Y);
     spritesheet = "assets/images/characters/Ninja.png";
     loadGraphic(spritesheet, true, 32, 32);

     loadDefaults();

     width = 12; height = 16;
     offset.x = 10; offset.y = 15;
     walkFactor = 1.25;

     loadAnimation();
     maxHealth = 2;
     health = maxHealth;

   }


   override public function update(elapsed:Float)
   {

     super.update(elapsed);

     switch(status)
     {
       case Status.SPECIAL01: special01Actions();
       case Status.SPECIAL02: special02Actions();
       case Status.SPECIAL03: special03Actions();
       default: null;
     }

   }


   override private function loadAnimation()
   {
     super.loadAnimation();
     animation.add(Status.ATTACK, [41,42], 8, false);
     animation.add(Status.WALKATTACK, [33,34,36,37], 8, false);
     animation.add(Status.JUMPATTACK, [41,42], 4, false);
     animation.add(Status.SPECIAL01, [49], 1, false);
     animation.add(Status.SPECIAL02, [50,49], 4, false);
     animation.add(Status.SPECIAL03, [57,58], 8, false);

     animation.callback = shot;

   }


   override private function jumpActions():Void
   {
     super.jumpActions();

     if (isTouching(FlxObject.WALL) &&
      FlxG.keys.anyJustPressed([FlxKey.W,FlxKey.UP]))
     {
       velocity.y = 0;
       acceleration.y = 0;
       status = Status.SPECIAL01;
       //wallgrab
     }

   }


   override private function fallActions():Void
   {
     super.fallActions();

     if (isTouching(FlxObject.WALL) &&
      FlxG.keys.anyJustPressed([FlxKey.W,FlxKey.UP]))
     {
       velocity.y = 0;
       status = Status.SPECIAL01;
       acceleration.y = 0;
       //wallgrab
     }

   }


   private override function hurtReset()
   {
     acceleration.y = CharacterBase.DGRAVITY;

   }


   //wallgrab
   private function special01Actions():Void
   {
     if (flipX &&
       FlxG.keys.anyPressed([FlxKey.D,FlxKey.RIGHT]) &&
       FlxG.keys.anyJustPressed([FlxKey.W,FlxKey.UP]))
     {
       status = Status.JUMP;
       velocity.y = CharacterBase.DJUMPSPEED-CharacterBase.DJUMPCUT;
       acceleration.y = CharacterBase.DGRAVITY;
       playSFX();
     }
     else if (!flipX &&
       FlxG.keys.anyPressed([FlxKey.A,FlxKey.LEFT]) &&
       FlxG.keys.anyJustPressed([FlxKey.W,FlxKey.UP]))
     {
       status = Status.JUMP;
       velocity.y = CharacterBase.DJUMPSPEED-CharacterBase.DJUMPCUT;
       acceleration.y = CharacterBase.DGRAVITY;
       playSFX();
     }
     else if (FlxG.keys.anyJustPressed([FlxKey.W,FlxKey.UP,FlxKey.S,FlxKey.DOWN]))
     {
       x+=(flipX?-0.1:0.1);
       status = Status.SPECIAL02;
     }
     else
     {
       x+=(flipX?-0.1:0.1);
     }

     if ((wasTouching != FlxObject.RIGHT)&&(wasTouching != FlxObject.LEFT))
     {
       //end wallgrab
       status = Status.IDLE;
       acceleration.y = CharacterBase.DGRAVITY;
     }

     if (FlxG.keys.anyJustPressed([FlxKey.H,FlxKey.SPACE]))
     {
       status = Status.SPECIAL03;
     }

   }

   //wallmovement
   private function special02Actions():Void
   {

     if (FlxG.keys.anyPressed([FlxKey.W,FlxKey.UP]))
     {
       velocity.y = -50;
       x+=(flipX?-0.1:0.1);
       if ((wasTouching != FlxObject.RIGHT)&&(wasTouching != FlxObject.LEFT))
       {
         status = Status.JUMP;
         acceleration.y = CharacterBase.DGRAVITY;
         playSFX();
       }
     }
     else if (FlxG.keys.anyPressed([FlxKey.S,FlxKey.DOWN]))
     {
       velocity.y = 50;
       x+=(flipX?-0.1:0.1);
       if ((wasTouching != FlxObject.RIGHT)&&(wasTouching != FlxObject.LEFT))
       {
         status = Status.FALL;
         acceleration.y = CharacterBase.DGRAVITY;
       }
     }
     else
     {
       velocity.y = 0;
       x+=(flipX?-0.1:0.1);
       status = Status.SPECIAL01;
     }

     if (FlxG.keys.anyJustPressed([FlxKey.H,FlxKey.SPACE]))
     {
       velocity.y = 0;
       status = Status.SPECIAL03;
     }

     if (wasTouching == FlxObject.FLOOR)
     {
       status = Status.IDLE;
       acceleration.y = CharacterBase.DGRAVITY;
     }

   }

   //wall attack
   private function special03Actions():Void
   {
     if (animation.curAnim.finished)
     {
       status = Status.SPECIAL02;
       x+=(flipX?-0.1:0.1);
       //restore wallgrab movement state
     }

   }


   private function shot(s:String,framenmb:Int,frameidx:Int)
   {
     if ( ((status == Status.ATTACK) || (status == Status.WALKATTACK) ||
      (status == Status.JUMPATTACK) || (status == Status.SPECIAL03)) &&
      (framenmb == (animation.curAnim.numFrames - 1)&&!animation.finished))
      {
        var ns = new NinjaShot(getMidpoint().x,y+4);
        if (animation.curAnim.name == Status.SPECIAL03)
          ns.velocity.x = (flipX?1:-1)*200;
        else
          ns.velocity.x = (!flipX?1:-1)*200;

        ns.facing = facing;
        Reg.playerShot.add(ns);

    }

  }


}
