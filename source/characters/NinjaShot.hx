package characters;

import flixel.FlxG;
import flixel.FlxObject;

/*
* Estrellita ninja, no pasa sobre muros y muere al tocar algo
*/

class NinjaShot extends PlayerShot
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    loadRotatedGraphic("assets/images/characters/ninjashot.png",8);

    loadAnimation();
    loadDefaults();

    solid = true;

  }


  override private function loadAnimation()
  {
    animation.add("normal",[0,1,2,3,4,5,6,7],true);

  }


  override private function loadDefaults()
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    animation.play("normal");

    playSFX();

  }


  override private function playSFX()
  {
    FlxG.sound.play("assets/sfx/ninjashot.ogg", 1, false);

  }

}
