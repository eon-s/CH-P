package characters;

import flixel.FlxObject;
import flixel.FlxG;
import flixel.system.FlxSound;

/*
* Rectángulo lleno de nada que logra un interesante y práctico trail de hitboxes,
* se ve muy bien en debug y con el Knight saltando
*/

@:allow(characters.Knight)
class KnightShot extends PlayerShot
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    this.makeGraphic(10,10,flixel.util.FlxColor.TRANSPARENT);
    timer = new flixel.util.FlxTimer();

    loadDefaults();

  }


  override public function hurt(damage:Float)
  {
    null;

  }


  override private function loadDefaults()
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    strength = 5;

    timer.start(0.08,function(tmr:flixel.util.FlxTimer){kill();});
    setSize(10,10);
    updateHitbox();

    attackSFX = FlxG.sound.load("assets/sfx/knightattack.ogg", 0.01, false);

  }


  override private function playSFX()
  {
    attackSFX.play();

  }


  private var timer:flixel.util.FlxTimer;
  private var attackSFX:FlxSound;

}
