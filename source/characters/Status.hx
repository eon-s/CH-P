package characters;

/*
* Lista de estados de los CharacterBase, utilizada para su rudimentaria FSM
*/

@:enum
abstract Status(String) from String to String
{
    var IDLE = "idle";
    var WALK = "walk";
    var JUMP = "jump";
    var FALL = "fall";
    var JUMPATTACK = "jumpattack";
    var WALKATTACK = "walkattack";
    var ATTACK = "attack";
    var DIE = "die";
    var SPECIAL01 = "special01";
    var SPECIAL02 = "special02";
    var SPECIAL03 = "special03";
}
