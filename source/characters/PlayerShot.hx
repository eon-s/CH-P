package characters;

import flixel.FlxSprite;
import flixel.FlxObject;

/*
* Disparos de los CharacterBase, son tratados aparte en el juego así que están separados,
* no como los disparos en enemies
*/

class PlayerShot extends FlxSprite
{

  public var ignoreWall:Bool;
  public var strength:Float;

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    ignoreWall = false;
    strength = 1;

  }


  public override function update(elapsed:Float)
  {
    if (!isOnScreen())
    {
      kill();
    }
    super.update(elapsed);
  }


  override public function revive()
  {
    super.revive();
    playSFX();
  }


  private function playSFX()
  {

  }


  private function loadAnimation():Void
  {

  }


  private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    playSFX();
  }

}
