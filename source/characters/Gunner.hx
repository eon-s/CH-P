package characters;

import flixel.FlxG;
import flixel.FlxObject;

/*
* Es lento para atacar, utiliza el disparo como excusa para un salto doble
*/

class Gunner extends CharacterBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    spritesheet = "assets/images/characters/Gunner.png";
    loadGraphic(spritesheet, true, 32, 32);

    loadDefaults();

    width = 10; height = 16;
    offset.x = 10; offset.y = 15;

    walkFactor = 0.85;
    jumpFactor = 0.95;

    loadAnimation();

    maxHealth = 3;
    health = maxHealth;

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);

    switch(status)
    {
      case Status.SPECIAL01: special01Actions();
      default: null;
    }

    if (status == Status.IDLE || status == Status.WALK)
      jumpStack = DMAXJUMPS;

  }


  override private function loadAnimation()
  {
   super.loadAnimation();
   animation.add(Status.ATTACK, [41,42,43,44,44,44,45], 8, false);
   animation.add(Status.WALKATTACK, [41,42,43,44,44,44,45], 8, false);
   animation.add(Status.JUMPATTACK, [41,42], 4, false);
   animation.add(Status.SPECIAL01, [49,50,51,51,52], 16, false);
   animation.callback = shot;

  }


  override private function walkActions():Void
  {
    super.walkActions();
    if (status == Status.WALKATTACK)
      velocity.x = 0;

  }


  override private function jumpActions():Void
  {
   super.jumpActions();
   if (status == Status.JUMPATTACK) //negate jump attack
    status = Status.JUMP;
    if (jumpStack>0 && status == Status.JUMP &&
      FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
    {
      status = Status.SPECIAL01;
      jumpStack--;
    }

  }


  override private function fallActions():Void
  {
   super.fallActions();
   if (status == Status.JUMPATTACK)
    status = Status.JUMP;

   if (jumpStack>0 && status == Status.FALL &&
     FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
   {
     status = Status.SPECIAL01;
     jumpStack--;
   }

  }


  override private function hurtReset()
  {
     jumpStack = 0;

  }


  override private function attackActions():Void
  {
   if (animation.curAnim.finished)
   {
     status = Status.WALK;
     velocity.x = 0;
   }

  }


  private function special01Actions()
  {
   if (animation.curAnim.finished)
   {
     velocity.y=CharacterBase.DJUMPSPEED*1.1;
     y-=0.1;
     status = Status.JUMP;
   }

  }


  private function shot(s:String,framenmb:Int,frameidx:Int)
  {
   if ( ((status == Status.ATTACK) || (status == Status.WALKATTACK) ||
    (status == Status.JUMPATTACK) || (status == Status.SPECIAL01)) &&
    (framenmb == (animation.curAnim.numFrames - 1)&&!animation.finished))
    {
      var gs = new GunnerShot(getMidpoint().x,y-24);

      if ((status == Status.SPECIAL01))
      {
        gs.angle = 180;

        if (flipX)
          gs.x-=40;
        else gs.x-=32;
          gs.y+=40;
      }
      else if (flipX)
      {
        gs.x-=76;
        gs.angle = -90;
        velocity.x = 100;
      }
      else
      {
        gs.x+=16;
        gs.angle = 90;
        velocity.x = -100;
      }

      Reg.playerShot.add(gs);
    }

  }


  private var jumpStack:Int = DMAXJUMPS;

  private static inline var DMAXJUMPS = 1;

}
