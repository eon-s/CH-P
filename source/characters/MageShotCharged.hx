package characters;

import flixel.FlxObject;
import flixel.FlxG;

/*
* Disparo de mago alternativo, no se destruye (y es más fuerte en caso de utilizar enemigos con más salud)
*/

class MageShotCharged extends MageShot
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    loadAnimation();
    loadDefaults();

    ignoreWall = true;

  }


  override public function hurt(damage:Float)
  {
    null;

  }


  override public function revive()
  {
    super.revive();
    playSFX();

  }


  override private function loadAnimation()
  {
    animation.add("normal",[1,2],true);

  }


  override private function loadDefaults()
  {
    width = 6; height = 6;
    offset.x = 10; offset.y = 5;

    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    acceleration.y = -80;
    animation.play("normal");
    strength = 3;

    playSFX();

  }


  override private function playSFX()
  {
    FlxG.sound.play("assets/sfx/magechargedshot.ogg", 1, false);

  }

}
