package characters;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;

/*
* Clase generica que maneja estados simples que admiten cierto nivel de abstraccion
* simplificando la implementacion de acciones.
*/

@:allow(gameui.Health)
class CharacterBase extends FlxSprite
{

  public var invulnerable = false;
  @:isVar public var status(get, null):Status;

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    maxHealth = 1;
    health = maxHealth;

  }


  override public function update(elapsed:Float)
  {
    // si está muriendo, no permitimos hacer nada
    if(isDead) return;

    //No utilizado pero esta pensado para posibilitar interrumpir interaccion en cutscenes
    if (!actorMode)
    {
      switch (status)
      {
        case Status.IDLE: idleActions();
        case Status.WALK: walkActions();
        case Status.JUMP: jumpActions();
        case Status.FALL: fallActions();
        case Status.ATTACK:attackActions();
        case Status.WALKATTACK:walkAttackActions();
        case Status.JUMPATTACK:jumpAttackActions();
        case Status.DIE:deadActions();
        default:null;
      }
      animation.play(status);
    }

    super.update(elapsed);

  }


  override public function kill()
  {
    allowCollisions = 0;
    velocity.y = -100;
    velocity.x = 0;
    status = Status.DIE;
    isDead = true;

    Reg.effectGroup.add(new effects.CharacterExplosion(x,y));

  }


  override public function hurt(damage:Float)
  {
    if (!invulnerable)
    {
      super.hurt(damage);

      if (damage>0)
      {
        flixel.util.FlxSpriteUtil.flicker(this,1,function(flk:flixel.effects.FlxFlicker){this.invulnerable = false;});
        invulnerable = true;
        x += (flipX?1:-1);
        y-=1;
        status = Status.FALL;
        hurtReset();
        FlxG.sound.play("assets/sfx/playerhurt.ogg", 1, false);
      }
      else if (damage<0)
      {
        if (health>maxHealth)
          health = maxHealth;
        else
          FlxG.sound.play("assets/sfx/healthup.ogg", 1, false);
      }

      if (health<0)
        health = 0;
    }

  }


  public function spikeHurt(damage:Float)
  {
    hurt(damage);

  }


  public function toggleActorMode()
  {
    if (actorMode)
    {
      actorMode = false;
      velocity.y = 0;
      velocity.x = 0;
      acceleration.y = DGRAVITY;
      status = Status.IDLE;
    }
    else
    {
      actorMode = true;
      velocity.y = 0;
      velocity.x = 0;
      acceleration.y = 0;
    }

  }


  private function loadAnimation():Void
  {
    animation.add(Status.IDLE, [9, 10, 11, 10, 9, 10, 11, 12], 4, true);
    animation.add(Status.WALK, [17, 18, 19], 10, true);
    animation.add(Status.JUMP, [25], 1, false);
    animation.add(Status.FALL, [26], 1, false);
    animation.add(Status.ATTACK, [41,42], 2, false);
    animation.add(Status.WALKATTACK, [41,42], 2, false);
    animation.add(Status.JUMPATTACK, [41,42], 2, false);
    animation.add(Status.DIE, [13], 1, true);

    animation.play(status);

  }

  private function loadDefaults():Void
  {
    status = Status.IDLE;

    acceleration.y = DGRAVITY;

    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

  }

  private function idleActions():Void
  {
    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.A,flixel.input.keyboard.FlxKey.LEFT]))
    {
      facing = FlxObject.LEFT;
      status = Status.WALK;
    }
    else if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.D,flixel.input.keyboard.FlxKey.RIGHT]))
    {
      facing = FlxObject.RIGHT;
      status = Status.WALK;
    }
    else if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
    {
      status = Status.JUMP;
      velocity.y = DJUMPSPEED*jumpFactor;
      playSFX();
    }
    else if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
    {
      status = Status.ATTACK;
      playSFX();
    }

  }

  private function walkActions():Void
  {
    if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.A,flixel.input.keyboard.FlxKey.LEFT]))
    {
      facing = FlxObject.LEFT;
      velocity.x = -DWALKSPEED*walkFactor;
    }
    else if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.D,flixel.input.keyboard.FlxKey.RIGHT]))
    {
      facing = FlxObject.RIGHT;
      velocity.x = DWALKSPEED*walkFactor;
    }
    else
    {
      status = Status.IDLE;
      velocity.x = 0;
    }

    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
    {
      status = Status.JUMP;
      velocity.y = DJUMPSPEED*jumpFactor;
      playSFX();
    }

    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
    {
      status = Status.WALKATTACK;
      velocity.x*=0.5;
      playSFX();
    }

    if (!isTouching(FlxObject.FLOOR))
    {
      status = Status.FALL;
    }

  }


  private function jumpActions():Void
  {
    if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.A,flixel.input.keyboard.FlxKey.LEFT]))
    {
      facing = FlxObject.LEFT;
      velocity.x = -DWALKSPEED*walkFactor;
    }
    else if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.D,flixel.input.keyboard.FlxKey.RIGHT]))
    {
      facing = FlxObject.RIGHT;
      velocity.x = DWALKSPEED*walkFactor;
    }
    else
    {
      velocity.x = 0;
    }

    if (FlxG.keys.anyJustReleased([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
      velocity.y = DJUMPCUT;

    if (velocity.y >= 0)
    {
      status = Status.FALL;
    }

    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
      {
        status = Status.JUMPATTACK;
        playSFX();
      }

  }


  private function fallActions():Void
  {
    if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.A,flixel.input.keyboard.FlxKey.LEFT]))
    {
      facing = FlxObject.LEFT;
      velocity.x = -DWALKSPEED*walkFactor;
    }
    else if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.D,flixel.input.keyboard.FlxKey.RIGHT]))
    {
      facing = FlxObject.RIGHT;
      velocity.x = DWALKSPEED*walkFactor;
    }
    else
    {
      velocity.x = 0;
    }

    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
      {
        status = Status.JUMPATTACK;
        playSFX();
      }

    if (justTouched(FlxObject.FLOOR))
    {
      status = Status.WALK;
      velocity.x = 0;
    }

  }


  private function attackActions():Void
  {
    if (animation.curAnim.finished)
    {
      status = Status.WALK;
      velocity.x = 0;
    }

  }


  private function walkAttackActions():Void
  {
    if (animation.curAnim.finished)
    {
      status = Status.WALK;
      velocity.x = 0;
    }

  }


  private function jumpAttackActions():Void
  {
    if (animation.curAnim.finished)
    {
      status = Status.FALL;
      y-=0.1;
    }

    if (justTouched(FlxObject.FLOOR))
    {
      velocity.x = 0;
    }

  }


  private function hurtReset():Void
  {

  }


  private function deadActions():Void
  {

  }


  private function playSFX()
  {
    switch (status)
    {
      case Status.JUMP: playJumpSFX();
      case Status.DIE:playDeadSFX();
      default:null;
    }

  }


  private function playJumpSFX()
  {
    FlxG.sound.play("assets/sfx/playerjump.ogg", 1, false);

  }

  private function playDeadSFX()
  {

  }


  private function get_status():Status  { return status;}
  private var isDead: Bool = false;
  private var spritesheet:String;

  private var walkFactor:Float = 1;
  private var jumpFactor:Float = 1;

  private var actorMode = false;
  private var maxHealth:Float;

  inline static var DGRAVITY:Float = 300;
  inline static var DWALKSPEED:Float = 100;
  inline static var DJUMPSPEED:Float = -150;
  inline static var DJUMPCUT:Float = -50;

}
