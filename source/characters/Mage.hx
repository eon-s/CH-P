package characters;

import flixel.FlxG;
import flixel.FlxObject;

/*
* Mago, utiliza su túnica para flotar, puede cargar su disparo y lanzar uno más potente
*/

class Mage extends CharacterBase
{

  public function new(X: Float, Y: Float)
  {
     super(X, Y);
     spritesheet = "assets/images/characters/Mage.png";
     loadGraphic(spritesheet, true, 32, 32);

     loadDefaults();

     width = 12; height = 16;
     offset.x = 10; offset.y = 15;

     loadAnimation();

     maxHealth = 3;
     health = maxHealth;

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);

    switch(status)
    {
      case Status.SPECIAL01: curFloatTime-=elapsed;
                             special01Actions();
      case Status.SPECIAL02: special02Actions();
      case Status.SPECIAL03: special03Actions();
      default: null;
    }

  }


  override private function loadAnimation()
  {
    super.loadAnimation();

    animation.add(Status.ATTACK, [41,42,43,43,43], 8, false);
    animation.add(Status.SPECIAL01, [49,50], 4, true);
    animation.add(Status.SPECIAL02, [57,58], 8, true);
    animation.add(Status.SPECIAL03, [57,44,42], 8, false);

  }


  override private function walkActions()
  {
    super.walkActions();

  }


  override private function walkAttackActions():Void
  {
    velocity.x=0;
    status = Status.ATTACK;
    attackActions();

  }


  override private function jumpActions()
  {
    super.jumpActions();
    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
    {
      status = Status.JUMP;
    }
    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
    {
      status = Status.SPECIAL01;
    }

  }


  override private function fallActions()
  {
    super.fallActions();
    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
    {
      status = Status.FALL;
    }
    if (status!=Status.FALL)
    {
      curFloatTime=DMAXFLOATTIME;
    }
    if (FlxG.keys.anyJustPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP]))
    {
      status = Status.SPECIAL01;
    }

  }


  override private function attackActions():Void
  {
    if (animation.curAnim.finished)
    {
      if (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
      {
        status = Status.SPECIAL02;
      }
      else
      {
        status = Status.SPECIAL03;
        shot();
      }

      velocity.x = 0;
    }

  }


  override private function hurtReset()
  {
    acceleration.y = CharacterBase.DGRAVITY;
    curFloatTime = DMAXFLOATTIME;

  }


  override private function playSFX()
  {
    switch (status)
    {
      case Status.ATTACK: null;
      case Status.WALKATTACK: null;
      case Status.SPECIAL02: playChargingSFX();
      default: super.playSFX();
    }

  }


  //float
  private function special01Actions()
  {
    if ((curFloatTime>=0)&&
      (FlxG.keys.anyPressed([flixel.input.keyboard.FlxKey.W,flixel.input.keyboard.FlxKey.UP])))
    {
      velocity.y=0;
    }
    else
    {
      y-=0.1;
      status = Status.FALL;
    }

  }

  //charge
  private function special02Actions()
  {
    playSFX();
    if (FlxG.keys.anyJustReleased([flixel.input.keyboard.FlxKey.H,flixel.input.keyboard.FlxKey.SPACE]))
    {
      status = Status.SPECIAL03;
      shotCharged();
    }

  }

  //shot
  private function special03Actions()
  {
    if (animation.curAnim.finished)
    {
      status = Status.WALK;
    }

  }


  private function shot(ofsx:Float = 0,ofsy:Float = 0)
  {
    var ms = new MageShot(getMidpoint().x+(!flipX?1:-1)*(4+ofsx),y+4+ofsy);
    ms.velocity.x = (!flipX?1:-1)*200;

    ms.facing = facing;
    Reg.playerShot.add(ms);

  }


  private function shotCharged()
  {
    var ms = new MageShotCharged(getMidpoint().x+(!flipX?4:-4),y+4);
    ms.velocity.x = (!flipX?1:-1)*200;

    ms.facing = facing;
    Reg.playerShot.add(ms);

    shot(-8,6);
    shot(-8,-6);

  }


  private function playChargingSFX()
  {
    FlxG.sound.play("assets/sfx/magecharging.ogg", 0.4, false);

  }


  private var curFloatTime = DMAXFLOATTIME;
  private static inline var DMAXFLOATTIME = 1.5;

}
