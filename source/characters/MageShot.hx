package characters;

import flixel.FlxObject;
import flixel.FlxG;

/*
* Disparo del mago, es caliente así que se desvía hacia arriba.
*/

class MageShot extends PlayerShot
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    loadGraphic("assets/images/characters/mageshot.png",true,16,16);
    loadAnimation();
    loadDefaults();
    solid = true;

  }


  override private function loadAnimation()
  {
    animation.add("normal",[6,7],true);

  }


  override private function loadDefaults()
  {
    width = 6; height = 6;
    offset.x = 10; offset.y = 5;

    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    acceleration.y = -80;
    animation.play("normal");
    ignoreWall = true;

    playSFX();

  }


  private override function playSFX()
  {
    FlxG.sound.play("assets/sfx/mageshot.ogg", 1, false);

  }

}
