package characters;

import flixel.FlxObject;
import flixel.FlxG;

/*
* El area del disparo es muy grande, así que está limitado a tres puntos de daño
* que se reducen con cada impacto.
*/

class GunnerShot extends PlayerShot
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    loadGraphic("assets/images/characters/gunnershot.png",true,64,64);

    loadAnimation();
    loadDefaults();
    solid = true;

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);
    if (animation.curAnim.finished)
      kill();

  }


  override public function hurt(damage:Float)
  {
    strength-=damage;
    if (strength<0)
      strength = 0;

  }


  override private function loadAnimation()
  {
    animation.add("normal",[0,1,2,3,4,5,6,7],false);

  }


  override private function loadDefaults()
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    animation.play("normal");
    strength = 3;

    playSFX();
  }


  override private function playSFX()
  {
    FlxG.sound.play("assets/sfx/gunnershot.ogg", 1, false);

  }

}
