package characters;

import flixel.group.FlxGroup;
import flixel.FlxObject;
import flixel.system.FlxSound;
import flixel.FlxG;

/*
 * Guarda los personajes a utilizar durante el juego, finalmente no se uso la opcion
 * pero es funcional (se puede activar con un "hack").
 */
class Bag extends FlxObject
{

  public function new()
  {
    super();
    characters = new FlxTypedGroup<CharacterBase>();
    characterHealth = new FlxTypedGroup<gameui.Health>();
    characterMusic = new Array<String>();
    var char:CharacterBase;

    current = 0;
    char = new Ninja(0,0);
    characters.add(char);
    char = new Gunner(0,0);
    characters.add(char);
    char.active = false;
    char = new Mage(0,0);
    characters.add(char);
    char.active = false;
    char = new Knight(0,0);
    characters.add(char);
    char.active = false;

    characterMusic.push("assets/bgm/BGM_Delta-Ninja.ogg");
    characterMusic.push("assets/bgm/BGM_Epsilon-Gunner.ogg");
    characterMusic.push("assets/bgm/BGM_Menu-Mage.ogg");
    characterMusic.push("assets/bgm/BGM_Gamma-Knight.ogg");

    for (i in 0...4)
      characterHealth.add(new gameui.Health(characters.members[i]));

  }


  public function change(N:Int):CharacterBase
  {
    characters.members[current].active = false;

    if ((characters.members[current].status == Status.IDLE)&&(current < characters.length))
    {
      characters.members[N-1].x = characters.members[current].x;
      characters.members[N-1].y = characters.members[current].y;
      characters.members[N-1].facing = characters.members[current].facing;
      current = N-1;
    }
    characters.members[current].active = true;
    FlxG.sound.playMusic(characterMusic[current],0.6,true);

    return characters.members[current];

  }


  public function activeChar():CharacterBase
  {
    return characters.members[current];

  }


  public function activeHealth():gameui.Health
  {
    return characterHealth.members[current];

  }


  private var characters:FlxTypedGroup<CharacterBase> ;
  private var characterHealth:FlxTypedGroup<gameui.Health> ;
  private var characterMusic:Array<String> ;
  private var current:Int;

}
