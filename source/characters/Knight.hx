package characters;

import flixel.FlxObject;
import characters.Status;

/*
* Es lento, salta bajo pero ignora obstacles.Spike y no tiene problemas en atacar corriendo
*/

class Knight extends CharacterBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    spritesheet = "assets/images/characters/Knight.png";
    loadGraphic(spritesheet, true, 32, 32);

    loadDefaults();

    width = 10; height = 16;
    offset.x = 10; offset.y = 15;
    walkFactor = 0.5;
    jumpFactor = 0.8;

    loadAnimation();

    maxHealth = 5;
    health = maxHealth;

  }


  override public function spikeHurt(damage:Float)
  {
    null;
  }


  override private function loadAnimation()
  {
    super.loadAnimation();
    animation.add(Status.ATTACK, [41,42,43,43,42], 10, false);
    animation.add(Status.WALKATTACK, [33,34,35,36], 12, false);
    animation.add(Status.JUMPATTACK, [41,42,43], 12, false);
    animation.add(Status.SPECIAL01, [49,49,50,50,50], 10, false);

  }


  override private function attackActions()
  {
    super.attackActions();
    var sht = cast(Reg.playerShot.recycle(KnightShot),KnightShot);
    if (sht != null) {sht.x = (flipX?-10:10)+x; sht.y = y+5; sht.loadDefaults();}

  }


  override private function jumpAttackActions()
  {
    super.jumpAttackActions();
    var sht = cast(Reg.playerShot.recycle(KnightShot),KnightShot);
    if (sht != null)
    {
      sht.x = (flipX?-10:10)+x; sht.y = y+5; sht.loadDefaults();
    }

  }


  override private function walkAttackActions()
  {
    super.walkAttackActions();
    var sht = cast(Reg.playerShot.recycle(KnightShot),KnightShot);
    if (sht != null)
    {
      sht.x = (flipX?-12:12)+x; sht.y = y+10; sht.loadDefaults();
    }

  }


}
