package enemies;

import flixel.FlxObject;
import flixel.FlxG;

/*
* Enemigo con problemas de reciclaje de disparos, se lo eliminó porque era muy molesto el efecto.
*/

class Cyclops extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner)
  {
    super(X, Y+8, s);

    spritesheet = "assets/images/enemies/cyclops.png";

    loadGraphic(spritesheet, true, 18, 16);
    height = 9;
    offset.y = 4;
    shotRand = new flixel.math.FlxRandom();
    shotTime = new flixel.util.FlxTimer();
    loadAnimation();
    loadDefaults();
    solid = true;
    ignoreWall = false;

    canDrop = true;

  }

  override public function update(elapsed:Float)
  {
    if (animation.curAnim.name == "shot")
    {
      if (animation.finished)
      {
        //Si aquí se utiliza reciclaje, los nuevos ThornShots aparecen en posición incorrecta
        // y si están sobre algo sólido se destruyen al instante
        animation.play("sleep");
        var newshot = new ThornShot(x+10,y+1);
        newshot.facing = FlxObject.RIGHT;
        newshot.setPosition(x+10,y+1);
        Reg.enemyGroup.add(newshot);
        newshot = new ThornShot(x+1,y+1);
        newshot.facing = FlxObject.LEFT;
        newshot.setPosition(x+1,y+1);
        Reg.enemyGroup.add(newshot);

        FlxG.sound.play("assets/sfx/enemyshot.ogg", 1, false);
      }
    }
    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("sleep", [3, 4, 5, 1], 4, false);
    animation.add("shot", [0,2], 4, false);
    animation.add("idle", [0], 1, false);
    animation.play("sleep");

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    animation.play("idle");

    shotTime.start(shotRand.float(2,4),shot,0);

  }


  private function shot(t:flixel.util.FlxTimer)
  {
    animation.play("shot");
    t.time = shotRand.float(2,4);

  }

  private var shotTime:flixel.util.FlxTimer;
  private var shotRand:flixel.math.FlxRandom;

}
