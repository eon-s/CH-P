package enemies;

import flixel.FlxG;

/*
* Spawner que solo funciona una vez al entrar en pantalla y siempre que su spawn esté muerto,
* eso ocasiona problemas con uno de los spawns...
*/

class NormalSpawner extends Spawner
{

  public function new(state:gamestates.CHPState,et:EnemyType,X:Float,Y:Float)
  {
    super(state,et,X,Y);

    running = false;
    spawns.maxSize = 1;

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);

    if (isOnScreen(FlxG.camera)&&!running&&spawns.countLiving()<1)
    {
      running = true;
      spawn();
    }

    running = isOnScreen(FlxG.camera);

  }


  private function spawn()
  {
    var spn:EnemyBase;

    switch ( enemyType )
    {
      case EnemyType.REDLEAK:
        spn = spawns.recycle(RedLeak,function():RedLeak{return new RedLeak(x,y,this);});
        spn.restart();
      case EnemyType.BLUEBIRD:
        spn = spawns.recycle(BlueBird,function():BlueBird{return new BlueBird(x,y,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      case EnemyType.BLUEBIRDGROUP:
        spn = spawns.recycle(BlueBirdGroup,function():BlueBirdGroup{return new BlueBirdGroup(x,y,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      case EnemyType.CYCLOPS:
        spn = spawns.recycle(Cyclops,function():Cyclops{return new Cyclops(x,y,this);});
        spn.restart();
      case EnemyType.ORANGEBIGBLOB:
        spn = spawns.recycle(OrangeBigBlob,function():OrangeBigBlob{return new OrangeBigBlob(x,y,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      case EnemyType.GREENTHING:
        spn = spawns.recycle(GreenThing,function():GreenThing{return new GreenThing(x,y,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      case EnemyType.SKELETON:
        spn = spawns.recycle(Skeleton,function():Skeleton{return new Skeleton(x-10,y-16,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      case EnemyType.BAT:
        spn = spawns.recycle(Bat,function():Bat{return new Bat(x-10,y,this,cast(FlxG.state,gamestates.PlayState).bag);});
        spn.restart();
      default:
        spn = spawns.recycle(RedLeak,function():RedLeak{return new RedLeak(x,y,this);});
        spn.restart();
        trace("Normal Spawner: Unsupported enemy type, creating default");
    }

    Reg.enemyGroup.add(spn);

  }


  private var running:Bool;

}
