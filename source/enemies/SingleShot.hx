package enemies;

import flixel.FlxObject;

/*
* Disparo sencillo
*/

class SingleShot extends EnemyBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    spritesheet = "assets/images/enemies/singleshot.png";
    loadGraphic(spritesheet);

    loadDefaults();
    solid = true;
    ignoreWall = true;
    ignoreShots = true;
    deadTouch = true;

  }

  override public function update(elapsed:Float)
  {
    if (justTouched(FlxObject.ANY))
    {
      kill();
    }

    super.update(elapsed);

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

  }

}
