package enemies;

import flixel.FlxObject;

/*
* Enemigo sencillo y molesto, cambia de dirección a cierta distancia X del jugador
*/

class Bat extends EnemyBase
{

  public var charging = false;

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    spritesheet = "assets/images/enemies/bat.png";
    super(X, Y, s, null, tg);

    loadGraphic(spritesheet, true, 16, 16);

    loadAnimation();
    loadDefaults();
    solid = true;
    ignoreWall = true;
    canDrop = true;

  }

  override public function update(elapsed:Float)
  {
    if (!charging&&(Math.abs(cast(target,characters.Bag).activeChar().getMidpoint().x-getMidpoint().x)<50))
    {
      flixel.math.FlxVelocity.moveTowardsObject(this,cast(target,characters.Bag).activeChar(),120);
      charging = true;
    }
    else if (!charging)
    {
      velocity.x = (!flipX?-1:1)*100;
    }

    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("fly", [0,1,2,1], 8, true);
    animation.play("fly");

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.LEFT, false, false);
    setFacingFlip(FlxObject.RIGHT, true, false);

    animation.play("fly");

  }

}
