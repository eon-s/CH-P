package enemies;

import flixel.FlxObject;

/*
* Por el comportamiento decidí considerarlo un enemigo, pero daña de manera negativa
* Es creado por algunos enemigos al morir.
*/

class Heart extends EnemyBase
{
  private var timer:flixel.util.FlxTimer;

  public function new(X: Float=0, Y: Float=0)
  {
    super(X, Y);

    spritesheet = "assets/images/enemies/heart.png";

    loadGraphic(spritesheet);
    loadDefaults();
    solid = true;
    ignoreWall = false;
    ignoreShots = true;
    deadTouch = true;
    acceleration.y = EnemyBase.DGRAVITY;

    offset.x-=2;
    offset.y-=2;
    width-=4;
    height-=2;
    updateHitbox();

  }


  override public function hurt(damage:Float)
  {
    x = -1;
    y = -1;
    timer.cancel();
    kill();

  }


  override public function revive()
  {
    super.revive();
    timer = new flixel.util.FlxTimer();
    timer.start(3,onTimerComplete);
    health = -1;
    velocity.y = 0;

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    timer = new flixel.util.FlxTimer();
    timer.start(3,onTimerComplete);
    health = -1;

  }


  private function onTimerComplete(t:flixel.util.FlxTimer)
  {
    flixel.effects.FlxFlicker.flicker(this,2,true, true,onFlickerComplete);

  }


  private function onFlickerComplete(f:flixel.effects.FlxFlicker)
  {
    hurt(1000);

  }


}
