package enemies;

import flixel.FlxObject;
import flixel.FlxG;

/*
* Pajarito multipropósito, utilizado por todos los spawners de modo solitario y en grupo
*/

class BlueBird extends EnemyBase
{

  public var angry = false;
  public var rushMode = true;

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    spritesheet = "assets/images/enemies/bluebird.png";
    super(X, Y, s, null, tg);
    loadGraphic(spritesheet, true, 16, 16);
    loadAnimation();
    loadDefaults();
    ignoreWall = true;
    solid = true;
    canDrop = true;

  }


  override public function update(elapsed:Float)
  {
    if (justTouched(FlxObject.WALL))
    {
      kill();
    }

    if (rushMode)
      updateDash();
    else
    {
      updateSpit();
    }
    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("fly", [0,1,2,3,2,1], 8, true);
    animation.add("rush", [5,4], 4, false);
    animation.add("spit", [5,5,6], 4, false);
    animation.play("fly");

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.LEFT, true, false);
    setFacingFlip(FlxObject.RIGHT, false, false);

    animation.play("fly");
    angry = false;

  }


  private function updateDash()
  {
    if (animation.curAnim.name == "fly")
    {
      if (target!=null)
      {
        facing = (startX > cast(target,characters.Bag).activeChar().x? FlxObject.RIGHT:FlxObject.LEFT);
      }
      if (angry||Math.abs(cast(target,characters.Bag).activeChar().getMidpoint().y-getMidpoint().y)<5)
      {
        animation.play("rush");
        deadTouch = true;
      }
    }

    if (animation.curAnim.name == "rush")
    {
      if (animation.finished)
      {
        velocity.x = (!flipX?-1:1)*150;
      }
    }

  }


  private function updateSpit()
  {
    if (animation.curAnim.name == "spit")
    {
      if (animation.finished)
      {
        var sshot = cast(Reg.enemyGroup.recycle(SingleShot,function(){return new SingleShot(x,y);}),SingleShot);
        sshot.setPosition(getMidpoint().x,getMidpoint().y);
        flixel.math.FlxVelocity.moveTowardsObject(sshot,cast(target,characters.Bag).activeChar(),150);

        Reg.enemyGroup.add(sshot);
        animation.play("fly");

        FlxG.sound.play("assets/sfx/enemyshot.ogg", 1, false);
      }
    }

  }

}
