package enemies;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.tile.FlxTilemap;

/*
* Base de todos los enemigos, disparos enemigos y Heart, todo lo que crea en cámara,
* se mueve y afecta de algún modo al jugador.
*/

class EnemyBase extends FlxSprite
{

  public var ignoreWall:Bool;
  public var ignoreShots:Bool;
  public var deadTouch:Bool;
  public var startX:Float;
  public var startY:Float;

  public function new(X: Float, Y: Float, ?s:Spawner=null, ?tmap:FlxTilemap = null, ?tg:FlxObject=null)
  {
    super(X, Y);
    startX = X;
    startY = Y;
    spawner = s;

    tilemap = tmap;
    target = tg;

    ignoreWall = true;
    ignoreShots = false;
    deadTouch = false;
    canDrop = false;

    defaultHealth = 1;
  }


  override public function update(elapsed:Float)
  {
    if(!isOnScreen(FlxG.camera))
    {
      alive = false;
      exists = false;
    }

    super.update(elapsed);

  }


  override public function hurt(damage:Float):Void
  {

    super.hurt(damage);
    if (health<=0)
    {
      var explosion:effects.EnemyExplosion= cast(Reg.effectGroup.recycle(effects.EnemyExplosion),effects.EnemyExplosion);

      explosion.scale.x = width/explosion.width;
      explosion.scale.y = width/explosion.width;

      explosion.x = x-12/scale.x;
      explosion.y = y-12/scale.x;

      if (canDrop)
        dropItem();
    }

  }


  override public function revive()
  {
    super.revive();
    health = defaultHealth;

  }


  public function restart()
  {
    reset(startX,startY);
    loadDefaults();

  }


  private function loadAnimation():Void
  {

  }


  private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    health = defaultHealth;

  }


  private function dropItem()
  {
    if (Reg.genericRNG.bool(10))
    {
      Reg.enemyGroup.add(new Heart(last.x,last.y));
    }

  }

  inline static var DGRAVITY:Float = 300;
  inline static var DWALKSPEED:Float = 100;
  inline static var DJUMPSPEED:Float = -150;
  private var defaultHealth:Float;
  private var walkFactor:Float = 1;
  private var jumpFactor:Float = 1;
  private var canDrop:Bool;
  private var spritesheet:String;
  private var spawner:Spawner;
  private var tilemap:FlxTilemap;
  private var target:FlxObject;

}
