package enemies;

import flixel.FlxObject;

/*
* Enemigo inmortal que crea RedDrops cada cierto tiempo
*/

class RedLeak extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner)
  {
    super(X, Y, s);

    spritesheet = "assets/images/enemies/redleak.png";
    loadGraphic(spritesheet, true, 16, 16);
    leakyTime = new flixel.util.FlxTimer();

    loadAnimation();
    loadDefaults();

    ignoreWall = false;
    deadTouch = false;
    ignoreShots = true;

  }


  public override function update(elapsed:Float)
  {
    if (animation.curAnim.name == "leak")
    {
      if (animation.finished)
      {
        animation.play("idle");

        var newDrop = cast(Reg.enemyGroup.recycle(RedDrop,function(){return new RedDrop(x,y);}),RedDrop);

        newDrop.restart();
        newDrop.reset(x,y);
      }
    }
    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("leak", [0, 1, 2, 3, 4, 3, 4], 4, false);
    animation.add("die", [4,5], 4, false);
    animation.add("idle", [0, 1, 2, 1], 4, true);
    animation.play("idle");

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    animation.play("idle");
    leakyTime.start(Reg.genericRNG.float(2,4),leak,0);
  }


  private function leak(t:flixel.util.FlxTimer)
  {
    animation.play("leak");
    t.time = Reg.genericRNG.float(2,4);

  }

  private var leakyTime:flixel.util.FlxTimer;

}
