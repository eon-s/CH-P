package enemies;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.group.FlxSpriteGroup;

/*
* Grupo que crea bluebirds y controla sus movimientos con un tween.
* También decide cuando el grupo de enemigos debe atacar.
* Es la pesadilla de los spawners, si bien logra un efecto megamanezco de que se rompan
* algunos spawns, necesita algunas correcciones para niveles como el 2.
*/

class BlueBirdGroup extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    super(X, Y, s, null, tg);

    visible = false;
    solid = false;

    spiteTime = new flixel.util.FlxTimer();
    spiteTime.start(Reg.genericRNG.float(2,4),spit,0);

    birdGroup = new FlxTypedSpriteGroup<BlueBird>();

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);

    if (birdGroup.countLiving()!=0)
    {
      alive = true;
    }
    else
    {
      groupMovement.cancel();
      x = startX;
      y = startY;
    }

  }


  override private function loadAnimation():Void
  {

  }


  override private function loadDefaults():Void
  {
    if (birdGroup.getFirstAlive()==null)
    {
      x = startX;
      y = startY;

      setFacingFlip(FlxObject.LEFT, true, false);
      setFacingFlip(FlxObject.RIGHT, false, false);

      birdGroup.x = x;
      birdGroup.y = y;

      var bb = new BlueBird(0,0,null,target);
      bb.rushMode = false;
      Reg.enemyGroup.add(bb);
      birdGroup.add(bb);

      bb = new BlueBird(0,-16, null,target);
      bb.rushMode = false;
      Reg.enemyGroup.add(bb);
      birdGroup.add(bb);

      bb = new BlueBird(0,16, null, target);
      bb.rushMode = false;
      Reg.enemyGroup.add(bb);
      birdGroup.add(bb);
    }
    groupMovement = flixel.tweens.FlxTween.tween(birdGroup,{y:y+48},2,{type:flixel.tweens.FlxTween.PINGPONG,onUpdate:onMotionTweenUpdate});

  }


  private function spit(t:flixel.util.FlxTimer)
  {
    birdGroup.forEach(function(e){
      e.animation.play("spit");
    });

  }


  private function onMotionTweenUpdate(t:flixel.tweens.FlxTween)
  {
    if (target!=null)
    {
      birdGroup.facing = (startX > cast(target,characters.Bag).activeChar().x? FlxObject.RIGHT:FlxObject.LEFT);
    }

  }


  private var birdGroup: flixel.group.FlxTypedSpriteGroup<BlueBird>;
  private var groupMovement:flixel.tweens.FlxTween;
  private var spiteTime:flixel.util.FlxTimer;

}
