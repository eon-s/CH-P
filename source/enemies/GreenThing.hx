package enemies;

import flixel.FlxObject;
import flixel.FlxG;

/*
* Enemigo que cada cierto tiempo dispara tres proyectiles en dirección al jugador
* Un disparo es directo, los otros dos con un ángulo
*/

class GreenThing extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    super(X, Y, s,tg);

    spritesheet = "assets/images/enemies/greenthing.png";
    loadGraphic(spritesheet, true, 16, 16);
    growRand = new flixel.math.FlxRandom();
    growTime = new flixel.util.FlxTimer();

    loadAnimation();
    loadDefaults();
    ignoreWall = false;
    canDrop = true;

  }


  override public function update(elapsed:Float)
  {

    if (animation.curAnim.name == "grow")
    {
      if (animation.finished)
      {
        animation.play("bloom");
        var newshot = cast(Reg.enemyGroup.recycle(GreenShot,function(){return new GreenShot(x+4,y);}),GreenShot);
        newshot.restart();
        newshot.setPosition(x+4,y);
        flixel.math.FlxVelocity.moveTowardsObject(newshot,cast(target,characters.Bag).activeChar(),100);

        var shotangle = flixel.math.FlxAngle.angleBetween(newshot,cast(target,characters.Bag).activeChar(),true);

        newshot = cast(Reg.enemyGroup.recycle(GreenShot,function(){return new GreenShot(x+4,y);}),GreenShot);
        newshot.restart();
        newshot.setPosition(x+4,y);
        newshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(shotangle+15,100);

        newshot = cast(Reg.enemyGroup.recycle(GreenShot,function(){return new GreenShot(x+4,y);}),GreenShot);
        newshot.restart();
        newshot.setPosition(x+4,y);
        newshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(shotangle-15,100);

        FlxG.sound.play("assets/sfx/enemyshot.ogg", 1, false);
      }
    }

    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("grow", [0, 1, 2, 3, 2], 3, false);
    animation.add("bloom", [4,5,6], 4, false);
    animation.add("idle", [6], 1, false);
    animation.play("idle");
  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    animation.play("idle");

    growTime.start(growRand.float(2,4),grow,0);

  }


  private function grow(t:flixel.util.FlxTimer)
  {
    animation.play("grow");
    t.time = growRand.float(2,4);
  }


  private var growTime:flixel.util.FlxTimer;
  private var growRand:flixel.math.FlxRandom;

}
