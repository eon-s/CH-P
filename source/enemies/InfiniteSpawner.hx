package enemies;

import flixel.FlxG;

/*
* Spawner que crea enemigos continuamente, se usan varias técnicas, la de skeleton
* puede tener problemas en algunas situaciones específicas (ej, cerca del final del nivel 3)
*/

class InfiniteSpawner extends Spawner
{

  public function new(state:gamestates.CHPState,et:EnemyType,X:Float,Y:Float)
  {
    super(state,et,X,Y);

    spawnTime = new flixel.util.FlxTimer();
    spawnRand = new flixel.math.FlxRandom();

  }


  override public function update(elapsed:Float)
  {
    super.update(elapsed);

    if (isOnScreen(FlxG.camera)&&!spawnTime.active)
    {
      spawnTime.start(2,spawn,1);
    }

  }


  private function spawn(t:flixel.util.FlxTimer)
  {
    var spn:EnemyBase;

    var point = new flixel.math.FlxPoint((spawnRand.bool()?0:FlxG.camera.width-8)+FlxG.camera.scroll.x,FlxG.camera.scroll.y+48);

    switch ( enemyType )
    {
      case EnemyType.BLUEBIRD:
        var endPoint = new flixel.math.FlxPoint(point.x,FlxG.camera.scroll.y+spawnRand.float(16,FlxG.camera.height-16));
        endPoint.y = FlxG.camera.scroll.y+spawnRand.float(16,FlxG.camera.height-16);
        spn = new BlueBird(endPoint.x,endPoint.y-20,this,cast(FlxG.state,gamestates.PlayState).bag);
        cast(spn,BlueBird).angry = true;
      case EnemyType.ORANGEBIGBLOB:
        point.y = FlxG.camera.scroll.y+spawnRand.float(16,FlxG.camera.height-16);
        spn = new OrangeBigBlob(point.x,point.y,this,cast(FlxG.state,gamestates.PlayState).bag);
      case EnemyType.SKELETON:
        var ground:flixel.tile.FlxTilemap = cast(cast(FlxG.state,gamestates.PlayState).level.solidTiles.members[0],flixel.tile.FlxTilemap);
        var endPoint = new flixel.math.FlxPoint(point.x,point.y+16*15);
        var resPoint = new flixel.math.FlxPoint();
        if (endPoint.y>ground.height)
          endPoint.y = ground.height;
        ground.ray(point,endPoint,resPoint);
        spn = new Skeleton(resPoint.x-10,resPoint.y-32,this,cast(FlxG.state,gamestates.PlayState).bag);
      case EnemyType.BAT:
        point.y = FlxG.camera.scroll.y+80;
        spn = new Bat(FlxG.camera.width-8+FlxG.camera.scroll.x,point.y,this,cast(FlxG.state,gamestates.PlayState).bag);
      default:
        spn = new OrangeBigBlob(x,y,this,cast(FlxG.state,gamestates.PlayState).bag);
        trace("Invalid spawn type for InfiniteSpawner. Spawning default enemy.");
    }

    spawns.add(spn);
    Reg.enemyGroup.add(spn);

  }

  private var spawnTime:flixel.util.FlxTimer;
  private var spawnRand:flixel.math.FlxRandom;

}
