package enemies;

import flixel.group.FlxSpriteGroup;
import flixel.group.FlxGroup;
import flixel.FlxState;
import flixel.FlxObject;

/**
 * Objeto Spawner que se activa al entrar en la cámara
 */
class Spawner extends FlxObject
{

  public function new(state:gamestates.CHPState,et:EnemyType,X:Float,Y:Float)
  {
    super();
    x = X;
    y = Y;
    width = 16;
    enemyType = et;
    spawns = new FlxTypedSpriteGroup<EnemyBase>();

  }

  private var enemyType:String;
  private var spawns:FlxTypedSpriteGroup<EnemyBase>;
  private var gamestate:gamestates.CHPState;

}
