package enemies;

import flixel.FlxObject;

/**
 * Enemigo con varias animaciones y movimientos controlados por temporizadores
 */
class Skeleton extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    super(X+10, Y+15, s, tg);

    spritesheet = "assets/images/enemies/skeleton.png";
    loadGraphic(spritesheet, true, 32, 32);

    defaultHealth = 1;

    loadAnimation();
    loadDefaults();

    ignoreWall = false;
    ignoreShots = false;
    deadTouch = true;
    canDrop = true;

  }


  override public function update(elapsed:Float)
  {

    if (deadTouch&&justTouched(FlxObject.WALL))
    {
      hurt(1000);
      actionTime.cancel();
    }
    else
    {
      if (jumping && velocity.y<0 && animation.curAnim.name == "jump")
        animation.play("fall");

      if (jumping && justTouched(FlxObject.FLOOR))
      {
        animation.play("land");
        velocity.x = 0;
      }

      if (jumping && animation.curAnim.name == "land" && animation.finished)
      {
        animation.play("walk");
        velocity.x = (flipX?-1:1)*50;
        jumping = false;
      }

      if (animation.curAnim.name == "attack" && animation.finished)
      {
          animation.play("walk");
          velocity.x = (flipX?-1:1)*50;
      }

    }

    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("dead", [4], 1, false);
    animation.add("rise", [4,5,6,7,0], 8, false);
    animation.add("think", [0,1,2,3], 8, true);
    animation.add("walk", [8,9,10,11,12,13], 8, true);
    animation.add("jump", [16], 8, false);
    animation.add("fall", [17,18,19,20], 8, false);
    animation.add("land", [21], 1, false);
    animation.add("attack", [24,25,26], 4, false);
    animation.play("dead");
  }

  override private function loadDefaults():Void
  {
    acceleration.y = EnemyBase.DGRAVITY;

    setFacingFlip(FlxObject.LEFT, false, false);
    setFacingFlip(FlxObject.RIGHT, true, false);
    animation.play("dead");
    velocity.x = 0;
    velocity.y = 0;
    width = 10;
    height = 15;
    offset.x = 10;
    offset.y = 15;
    x = startX;
    y = startY;

    actionRand = new flixel.math.FlxRandom();
    actionTime = new flixel.util.FlxTimer();

    actionTime.start(actionRand.float(0.1,1),action,5);
    facing = (startX > cast(target,characters.Bag).activeChar().x? FlxObject.RIGHT:FlxObject.LEFT);

  }


  private function action(t:flixel.util.FlxTimer)
  {
    switch(t.elapsedLoops)
    {
      case 1:
        animation.play("rise");
        t.time = 1;
      case 2:
        animation.play("think");
        t.time = actionRand.float(0.1,1);
      case 3:
        willJump = actionRand.bool();
        if (willJump)
          t.time = actionRand.float(1,2);
        animation.play("walk");
        velocity.x = (flipX?-1:1)*50;
      case 4:
        if (willJump&&wasTouching==FlxObject.FLOOR)
        {
          animation.play("jump");
          jumping = true;
          velocity.y = -150;
        }
        willJump = false;
        willAttack = actionRand.bool();
        t.time = actionRand.float(1,3);
      case 5:
        if (willAttack)
        {
          if (animation.curAnim.name == "walk")
          {
            animation.play("attack");
            velocity.x = 0;
          }
        }
      default: null;
    }

  }

  private var willJump:Bool = false;
  private var willAttack:Bool = false;
  private var jumping:Bool = false;
  private var actionRand:flixel.math.FlxRandom;
  private var actionTime:flixel.util.FlxTimer;

}
