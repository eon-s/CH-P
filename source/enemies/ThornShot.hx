package enemies;

import flixel.FlxG;
import flixel.FlxObject;

/*
* Disparo creado por los enemies.Cyclops
*/

class ThornShot extends EnemyBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    spritesheet = "assets/images/enemies/thornshot.png";
    loadGraphic(spritesheet);

    loadDefaults();

    ignoreWall = false;
    deadTouch = true;
    ignoreShots = false;
  }


  override public function update(elapsed:Float)
  {

    velocity.x = (flipX?-1:1)*100;
    if (deadTouch&&justTouched(FlxObject.ANY))
    {
      hurt(1000);
    }

    super.update(elapsed);

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

    reset(startX,startY);

  }

}
