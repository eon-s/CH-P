package enemies;

import flixel.FlxObject;

/*
* Disparo sencillo que puede ser destruído por el jugador
*/

class GreenShot extends EnemyBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);

    spritesheet = "assets/images/enemies/greenshot.png";
    loadGraphic(spritesheet,true,7,7);

    loadAnimation();
    loadDefaults();
    solid = true;
    ignoreWall = true;
    deadTouch = true;

  }

  override public function update(elapsed:Float)
  {
    if (justTouched(FlxObject.ANY))
    {
      kill();
    }
    super.update(elapsed);

  }

  override private function loadAnimation():Void
  {
    animation.add("idle", [0, 1, 2, 3], 8, true);
    animation.play("idle");
  }

  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);

  }

}
