package enemies;

import flixel.FlxObject;

/*
* Otro enemigo que sufre los efectos del reciclaje de disparos, no se emparchó
* el problema para que se pueda apreciar (es muy irregular).
*/

class OrangeBigBlob extends EnemyBase
{

  public function new(X: Float, Y: Float,?s:Spawner,?tg:FlxObject)
  {
    super(X, Y, s, tg);

    spritesheet = "assets/images/enemies/orangebigblob.png";
    loadGraphic(spritesheet, true, 16, 16);
    scale.x=1.25;
    scale.y=1.25;

    defaultHealth = 1;

    loadAnimation();
    loadDefaults();

    solid = true;
    ignoreWall = true;
    deadTouch = true;
    ignoreShots = false;

  }


  override public function hurt(damage:Float)
  {
    super.hurt(damage);

    if (health<=0)
    {
      //Algunas veces, disparos reciclados aparecen sobre el jugador, no encuentro la razón
      var sshot = cast(Reg.enemyGroup.recycle(SingleShot,function(){return new SingleShot(0,0);}),SingleShot);
      sshot.setPosition(getMidpoint().x,getMidpoint().y);
      sshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(45,150);

      sshot = cast(Reg.enemyGroup.recycle(SingleShot,function(){return new SingleShot(0,0);}),SingleShot);
      sshot.setPosition(getMidpoint().x,getMidpoint().y);
      sshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(135,150);

      sshot = cast(Reg.enemyGroup.recycle(SingleShot,function(){return new SingleShot(0,0);}),SingleShot);
      sshot.setPosition(getMidpoint().x,getMidpoint().y);
      sshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(-45,150);

      sshot = cast(Reg.enemyGroup.recycle(SingleShot,function(){return new SingleShot(0,0);}),SingleShot);
      sshot.setPosition(getMidpoint().x,getMidpoint().y);
      sshot.velocity = flixel.math.FlxVelocity.velocityFromAngle(-135,150);
    }
    
  }


  override public function update(elapsed:Float)
  {
    flixel.math.FlxVelocity.moveTowardsObject(this,cast(target,characters.Bag).activeChar(),30);
    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("idle", [0, 1, 2, 3], 4, true);
    animation.play("idle");

  }


  override private function loadDefaults():Void
  {
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    animation.play("idle");

  }


  private var enemyGroup: flixel.group.FlxGroup;

}
