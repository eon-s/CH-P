package enemies;

/*
* Especie de enum usado por los spawners
*/

@:enum
abstract EnemyType(String) from String to String
{
    var REDLEAK = "redleak";
    var BLUEBIRDGROUP = "bluebirdgroup";
    var BLUEBIRD = "bluebird";
    var CYCLOPS = "cyclops";
    var ORANGEBIGBLOB = "orangebigblob";
    var GREENTHING = "greenthing";
    var SKELETON = "skeleton";
    var BAT = "bat";
    var HEART = "heart";

}
