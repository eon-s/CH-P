package enemies;

import flixel.FlxObject;

/*
* Enemigos creados por RedLeaks, solo caen y mueren, también se destruyen si tocan al jugador
*/

class RedDrop extends EnemyBase
{

  public function new(X: Float, Y: Float)
  {
    super(X, Y);
    spritesheet = "assets/images/enemies/reddrop.png";
    loadGraphic(spritesheet, true, 16, 16);

    loadAnimation();
    loadDefaults();
    ignoreWall = false;
    deadTouch = true;
    ignoreShots = true;
  }


  override public function update(elapsed:Float)
  {

    if (wasTouching == FlxObject.DOWN)
    {
      animation.play("crash");
      acceleration.y = 0;
    }

    if (animation.curAnim.name == "crash")
    {
      if (animation.finished) kill();
    }

    super.update(elapsed);

  }


  override private function loadAnimation():Void
  {
    animation.add("crash", [1,2,3,4,5,6,7], 12, false);
    animation.add("fall", [0], 1, false);
    animation.play("fall");

  }


  override private function loadDefaults():Void
  {
    acceleration.y = EnemyBase.DGRAVITY;
    setFacingFlip(FlxObject.RIGHT, false, false);
    setFacingFlip(FlxObject.LEFT, true, false);
    animation.play("fall");

  }

}
