﻿package;

/*
* UNL-FRSF-TDPV Programacion 2
* Trabajo Final
*
*	EONS
*
* Fecha: 15/06/2016
*
* Se utilizaron técnicas variadas, no totalmente consistentes.
* No se aprovechó al máximo el reciclaje por problemas con el mismo.
*
* La idea era que sea similar al juego Little Samson (cambio de personajes durante el juego)
*	pero se bloqueó la opcion en estos niveles para que quede mas presentable, dejando
* abierta la posibilidad de continuarlo en algun momento (se puede desbloquear con
* la tecla NUEVE).
*
* Otra característica es un intento de imitar los spawners de Megaman, incluyendo sus
* errores, aunque no se consiguio reproducirlos fielmente.
*
* El juego consta de 6 partes, introduccion, 4 niveles y final.
*
*
* Mi código y mis assets estan en dominio público/CC0
*
*/

import flixel.FlxGame;
import openfl.display.Sprite;
import gamestates.*;

class Main extends Sprite
{
	public function new()
	{
		super();
		Reg.loadLevelList();
		addChild(new FlxGame(320, 240, MenuState));
	}
}
