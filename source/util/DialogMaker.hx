package util;

import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxColor;

/*
* Un creador de diálogos, utilizado principalmente en las cutscenes
*/

class DialogMaker
{

  public static function makeDialog(S:FlxSprite, col:Int, row:Int, elements:Array<Int>, duration:Int = 2):FlxSprite
  {

    var spr = new Dialog(S,duration);

    spr.offset.y = 16*row+24;
    spr.makeGraphic(16*col+16, 16*row+24, FlxColor.TRANSPARENT,true);

    FlxSpriteUtil.drawRoundRect(spr,0,0,16*col+16,16*row+16,16,16,FlxColor.WHITE,{ color: FlxColor.BLACK, thickness: 2 });
    FlxSpriteUtil.drawPolygon(spr,[ new FlxPoint(8,16*row+16-1),
                                    new FlxPoint(8,16*row+24),
                                    new FlxPoint(16,16*row+16-1)],
                                  FlxColor.WHITE);
    FlxSpriteUtil.drawLine(spr,8,16*row+16,8,16*row+24,{ color: FlxColor.BLACK, thickness: 1 });
    FlxSpriteUtil.drawLine(spr,8,16*row+24,16,16*row+16,{ color: FlxColor.BLACK, thickness: 1 });

    for (j in 0...row)
      for (i in 0...col)
      {
        try
        {
          if ((i+j*col<elements.length)&&elements[i+j*col]!=-1)
          {
            var emj = new FlxSprite("assets/images/emj/"+elements[i+j*col]+".png");
            spr.stamp(emj,8+16*i,8+16*j);
          }
        }
        catch(msj:String)
        {
          trace(msj);
        }
      }

    return spr;

  }


  private static var emoji:Map<Int, String> =
      [
      1 => "one",
      2 => "two",
      99 => "assets/images/emj/99.png"
      ];

}
