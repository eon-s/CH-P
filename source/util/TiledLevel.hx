package util;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.addons.editors.tiled.TiledImageLayer;
import flixel.addons.editors.tiled.TiledImageTile;
import flixel.addons.editors.tiled.TiledLayer.TiledLayerType;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.group.FlxGroup;
import flixel.tile.FlxTilemap;
import haxe.io.Path;

/**
 * Original de Samuel Batista para demo de HaxeFlixel, adaptado a este juego
 * Se extiende de TiledMap y hace un buen uso de todos los nuevos elementos de HaxeFlixel 4 y addons 2
 * Tiene agregados niveles extra de background y foreground sin parallax y otros elementos particulares de este juego.
 */
@:allow(gamestates.CHPState)
@:allow(enemies.Spawner)

class TiledLevel extends TiledMap
{

	public function new(tiledLevel:Dynamic, state:gamestates.CHPState)
	{
		super(tiledLevel);

		isCutscene = this.properties.contains("cutscene");

		imagesLayer = new FlxGroup();
		solidTiles = new FlxGroup();
		bkgObjectsLayer = new FlxGroup();
		frgObjectsLayer = new FlxGroup();
		backgroundLayer = new FlxGroup();
		foregroundLayer = new FlxGroup();

		FlxG.camera.setScrollBoundsRect(0, 0, fullWidth, fullHeight, true);

		loadImages();

		loadObjects(state);
		// Load Tile Maps
		for (layer in layers)
		{
			if (layer.type != TiledLayerType.TILE) continue;
			var tileLayer:TiledTileLayer = cast layer;

			var tileSheetName:String = tileLayer.properties.get("tileset");

			if (tileSheetName == null)
				throw "'tileset' property not defined for the '" + tileLayer.name + "' layer. Please add the property to the layer.";

			var tileSet:TiledTileSet = null;
			for (ts in tilesets)
			{
				if (ts.name == tileSheetName)
				{
					tileSet = ts;
					break;
				}
			}

			if (tileSet == null)
				throw "Tileset '" + tileSheetName + " not found. Did you misspell the 'tilesheet' property in " + tileLayer.name + "' layer?";

			var imagePath 		= new Path(tileSet.imageSource);
			var processedPath 	= c_PATH_LEVEL_TILESHEETS + imagePath.file + "." + imagePath.ext;

			var tilemap:FlxTilemap = new FlxTilemap();
			tilemap.pixelPerfectRender = true;
			tilemap.loadMapFromArray(tileLayer.tileArray, width, height, processedPath,
				tileSet.tileWidth, tileSet.tileHeight, OFF, tileSet.firstGID, 1, 1);

			if (tileLayer.properties.contains("nocollide"))
			{
				tilemap.solid = false;
				tilemap.active = false;
				if (tileLayer.properties.contains("foreground"))
					foregroundLayer.insert(0,tilemap);
				else
					backgroundLayer.add(tilemap);
			}
			else
			{
				if (collidableTileLayers == null)
					collidableTileLayers = new Array<FlxTilemap>();

				solidTiles.add(tilemap);
				collidableTileLayers.push(tilemap);
			}
		}

		if (properties.contains("lockedbag"))
		{
			var pState = cast(state,gamestates.PlayState);
			pState.lockedBag = true;
			pState.forceCharacter(Std.parseInt(properties.get("lockedbag")));
		}

	}


	public function collideWithLevel(obj:flixel.FlxBasic, ?notifyCallback:FlxObject->FlxObject->Void, ?processCallback:FlxObject->FlxObject->Bool):Bool
	{
		if (collidableTileLayers == null)
			return false;

		for (map in collidableTileLayers)
		{
			// IMPORTANT: Always collide the map with objects, not the other way around.
			//			  This prevents odd collision errors (collision separation code off by 1 px).
			if (FlxG.overlap(map, obj, notifyCallback, processCallback != null ? processCallback : FlxObject.separate))
			{
				return true;
			}
		}
		return false;

	}


	private function loadObjects(state:gamestates.CHPState)
	{
		var layer:TiledObjectLayer;
		var bkObjectLayer = new FlxGroup();
		var fgObjectLayer = new FlxGroup();
		for (layer in layers)
		{
			if (layer.type != TiledLayerType.OBJECT)
				continue;
			var objectLayer:TiledObjectLayer = cast layer;

			//collection of images layer
			if (layer.name == "images")
			{
				for (o in objectLayer.objects)
				{
					loadImageObject(o);
				}
			}

			//objects layer
			if (layer.name == "objects")
			{
				for (o in objectLayer.objects)
				{
					loadObject(state, o, objectLayer, bkgObjectsLayer);
				}
			}

			if (layer.name == "obstacles")
			{
				for (o in objectLayer.objects)
				{
					loadObstacle(state, o,frgObjectsLayer);
				}
			}
		}

	}


	private function loadImages()
	{
		for (layer in layers)
		{
			if (layer.type != TiledLayerType.IMAGE)
				continue;

			var image:TiledImageLayer = cast layer;

			trace(c_PATH_LEVEL_TILESHEETS + image.imagePath);

			var sprite = new FlxSprite(image.x, image.y, "assets/images/maps/country-platform-back.png");

			imagesLayer.add(sprite);
		}

	}


	private function loadImageObject(object:TiledObject)
	{
		var isForeground=false;
		var tilesImageCollection:TiledTileSet = this.getTileSet("imageCollection");
		var path = tilesImageCollection.getPropertiesByGid(object.gid).get("path");
		var decoSprite:FlxSprite = new FlxSprite(0, 0, path);

		if (decoSprite.width != object.width || decoSprite.height != object.height)
		{
			decoSprite.antialiasing = true;
			decoSprite.setGraphicSize(object.width, object.height);
		}
		decoSprite.setPosition(object.x, object.y - decoSprite.height);
		decoSprite.origin.set(0, decoSprite.height);
		if (object.angle != 0)
		{
			decoSprite.angle = object.angle;
			decoSprite.antialiasing = true;
		}

		//Custom Properties
		if (object.properties.contains("depth"))
		{
			var depth = Std.parseFloat( object.properties.get("depth"));
			decoSprite.scrollFactor.set(depth,depth);
			isForeground = depth > 1;
		}

		decoSprite.solid = false;
		decoSprite.active = false;
		if (isForeground)
			foregroundLayer.add(decoSprite);
		else
			backgroundLayer.add(decoSprite);

	}


	private function loadObstacle(state, object:TiledObject,group:FlxGroup)
	{
		var isForeground=false;
		var tilesImageCollection:TiledTileSet = this.getTileSet("obstacleCollection");
		var path = tilesImageCollection.getPropertiesByGid(object.gid).get("path");
		var tempState = cast(state,gamestates.PlayState);

		switch (object.type)
		{
			case "spike":
							var spk = new obstacles.Spike(object.x, object.y, object.angle, path);

							tempState.damageZones.add(spk);
							group.add(spk);
			default: null;
		}

	}


	private function loadObject(state:gamestates.CHPState, o:TiledObject, g:TiledObjectLayer, group:FlxGroup)
	{
		var x:Int = o.x;
		var y:Int = o.y;

		// objects in tiled are aligned bottom-left (top-left in flixel)

		if (o.gid != -1)
			y -= g.map.getGidOwner(o.gid).tileHeight;

		if (isCutscene)
		{
			switch (o.type.toLowerCase())
			{
				default: null;
			}
		}
		else
		{
			var tempState = cast(state,gamestates.PlayState);
			switch (o.type.toLowerCase())
			{
				case "player_start":
					var player = tempState.bag.activeChar();

					player.x = o.x;
					player.y = o.y-17;
					FlxG.camera.follow(player);
					tempState.player = player;

				case "killzone":
					var kz = new FlxObject(x, y, o.width, o.height);

					tempState.killzone.add(kz);
					group.add(kz);

				case "impassable":
					var wall = new FlxObject(x, y, o.width, o.height);

					tempState.levelLimits.add(wall);
					group.add(wall);

				case "spawner":
					var spawner = new enemies.NormalSpawner(tempState,o.properties.get("spawntype"),x, y);

					tempState.spawners.add(spawner);
					group.add(spawner);

				case "infinitespawner":
					var spawner = new enemies.InfiniteSpawner(tempState,o.properties.get("spawntype"),x, y);

					tempState.spawners.add(spawner);
					group.add(spawner);

				case "exit":
					var exit = new obstacles.Exit(x, y, o.height, o.width, Std.parseInt(o.properties.get("nextLevel")),o.properties.contains("isCutscene"));

					tempState.exit = exit;
					group.add(exit);
				default: null;
			}
		}

	}


	private inline static var c_PATH_LEVEL_TILESHEETS = "assets/images/maps/";

	private var solidTiles:FlxGroup;
	private var bkgObjectsLayer:FlxGroup;
	private var frgObjectsLayer:FlxGroup;
	private var backgroundLayer:FlxGroup;
	private var foregroundLayer:FlxGroup;
	private var collidableTileLayers:Array<FlxTilemap>;
	private var imagesLayer:FlxGroup;

	private var isCutscene:Bool;

}
