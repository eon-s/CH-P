package util;

import flixel.FlxSprite;

/*
* Globo de diálogo de tamaño dinámico que muestra recuadros con emojis, muy usado en cutscenes
*/

class Dialog extends FlxSprite
{

  public var owner:FlxSprite;

  public function new(S:FlxSprite, T:Float = 2.0)
  {
    super();
    owner = S;
    timeout = T;
    timer.active = false;

  }


  public override function update(elapsed:Float)
  {
    x = owner.x-owner.offset.x;
    y = owner.y-owner.offset.y;
    if (!timer.active)
    {
      timer.active = true;
      timer.start(timeout,function(t){kill();},1);
    }

  }


  private var timeout:Float;
  private var timer = new flixel.util.FlxTimer();

}
