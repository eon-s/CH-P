package gameui;

import flixel.FlxSprite;

/*
* Grupo de corazones que se muestran en pantalla, a cada character le corresponde uno.
* Representa el nivel de health del character asignado.
*/

class Health extends flixel.group.FlxSpriteGroup
{

  public function new(tg:characters.CharacterBase)
  {
    super(10, 10);
    target = tg;
    scrollFactor.x = 0;
    scrollFactor.y = 0;

    var spritesheet = "assets/images/UI/UIheart.png";
    var hrt:FlxSprite;

    for (i in 0...Std.int(target.maxHealth))
    {
      hrt = new FlxSprite();
      hrt.loadGraphic(spritesheet,true,16,16);
      hrt.animation.add("empty",[0],1,false);
      hrt.animation.add("full",[1],1,false);
      add(hrt);
      hrt.y=8+i*16+2;
    }

  }


  override public function update(elapsed:Float)
  {
    if (target.health>=0)
    {
      for (i in 0...Std.int(target.health))
      {
        members[i].animation.play("full");
      }
      for (i in Std.int(target.health)...Std.int(target.maxHealth))
      {
      members[i].animation.play("empty");
      }
    }

    super.update(elapsed);

  }

  private var target:characters.CharacterBase;

}
