package effects;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.system.FlxSound;

/*
* Explosión simple, enemies.EnemyBase se encarga del escalado y ubicación
*/

class EnemyExplosion extends FlxSprite
{

  public function new(X: Float=0, Y: Float=0)
  {
    super(X, Y);
    var spritesheet = "assets/images/effects/enemyExplosion.png";
    loadGraphic(spritesheet,true,32,32);
    loadAnimation();
    solid = false;

    explosionSFX = FlxG.sound.load("assets/sfx/enemyexplosion.ogg", 0.6, false);

    explosionSFX.setPosition(x, y);
    explosionSFX.play();

  }


  override public function update(elapsed:Float)
  {
    if (animation.curAnim.finished)
      kill();
    super.update(elapsed);

  }


  override public function revive()
  {
    super.revive();
    animation.play("unique");

    explosionSFX.setPosition(x, y);
    explosionSFX.play();

  }


  private function loadAnimation():Void
  {
    animation.add("unique",[0,1,2,3,4,5,6,7],false);
    animation.play("unique");

  }

  private var explosionSFX:FlxSound;

}
