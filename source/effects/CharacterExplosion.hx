package effects;

import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.FlxG;

/*
* Explosión que aparece cuando muere el jugador
*/

class CharacterExplosion extends FlxSpriteGroup
{

  public function new(X: Float=0, Y: Float=0)
  {
    super(X, Y);
    // cargamos la spritesheet del personaje
    var spritesheet = "assets/images/intro/bag.png";

    var xpl = new FlxSprite(0,0,spritesheet);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(0,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(45,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(90,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(135,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(180,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(-45,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(-90,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(-135,100);
    add(xpl);
    xpl = new FlxSprite(0,0,spritesheet);
    xpl.velocity = flixel.math.FlxVelocity.velocityFromAngle(-180,100);
    add(xpl);

    FlxG.sound.play("assets/sfx/characterexplosion.ogg", 1, false);

  }

}
